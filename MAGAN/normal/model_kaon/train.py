"""
Training implementation
"""

__version__= '1.1'
__author__ = 'Ifan Williams, Saúl Alonso-Monsalve, Leigh Howard Whitehead'
__email__= 'ifan.williams@cern.ch, saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'

import os
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# here one can specify the GPUs to use
os.environ['CUDA_DEVICE_ORDER']=''
os.environ['CUDA_VISIBLE_DEVICES']='1' 
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)
# Import relevant packages
import time
import math
import numpy as np
import random as rn
import pandas as pd
import matplotlib.pyplot as plt
from functions import plot_history, plot_deltaDLL, plot_inputs, plot_outputs, plot_correlation, plot_DLL, histogram_intersection
from networks import Networks
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.models import Model
from tensorflow.python.keras import metrics
from tensorflow.python.keras.models import model_from_json
from sklearn.preprocessing import QuantileTransformer
from sklearn.utils import shuffle
from scipy.interpolate import interp1d
from sklearn.externals import joblib
from argparse import ArgumentParser

# Ranges for the transformations
net_range = [-1,1]
gauss_range = [-5.5,5.5]

# Mapping functions
mapping = interp1d(gauss_range, net_range)
antimapping = interp1d(net_range, gauss_range)

# Remove for randomness - setting these (should) give reproducible results
np.random.seed(7)
rn.seed(15453)
tf.set_random_seed(1412)
session_conf = tf.ConfigProto(intra_op_parallelism_threads=1,
                              inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

def get_args():
    ''' Retrieve arguments.

    Returns: arguments.
    '''
    parser = ArgumentParser(description='Model-Assisted GAN')
    arg = parser.add_argument
    arg('--params_physics', type=int, default=13, help='Physics parameters')
    arg('--params_noise', type=int, default=51, help='Noise size (generator input)')
    arg('--observables', type=int, default=6, help='Number of observables')
    arg('--train', type=lambda x: (str(x).lower() == 'true'), default=True, help='Run training')
    arg('--train_steps', type=int, default=15001, help='Training steps')
    arg('--batch_size', type=int, default=32, help='Batch size')
    arg('--train_no', type=str, default=1, help='Training number')
    arg('--print_summary', type=lambda x: (str(x).lower() == 'true'), default=False, help='Print summary of the models')
    arg('--data_path', type=str, default='/home/iw273/rich-pid-fastsim-model-assisted-gan/data/PID-train-data-KAONS_newDLL_disampled_nocopy_newbinning.hdf', help='Data file path')
    arg('--save_weights', type=lambda x: (str(x).lower() == 'true'), default=True, help='Save weights')
    arg('--output_path', type=str, default='/home/iw273/rich-pid-fastsim-model-assisted-gan/model_kaon/outputs', help='Output path')
 
    args = parser.parse_args()
    return args

args = get_args()

# Set up class

class ModelAssistedGANPID(object):

	def __init__(self, params=64, observables=6, print_summary=False):
		''' Constructor.
        
        Args:
            params: total number of physics + noise parameters.
            observables: size of output DLL vector.
            print_summary: print summary of the models.
        '''

		self.params = params
		self.observables = observables
		self.Networks = Networks(params=params, observables=observables, print_summary=print_summary)
		self.emulator = self.Networks.emulator()
		self.siamese = self.Networks.siamese_model()
		self.adversarial1 = self.Networks.adversarial1_model()
		#self.discriminator = self.Networks.discriminator_model()
		#self.adversarial2 = self.Networks.adversarial2_model()
		#self.generator = self.Networks.generator()
		#self.generator_emulator = self.Networks.generator_emulator()

	# Function to save model weights
	def save_weights(self, path='output', train_no=1):
		''' Save model weights.

        Args:
            path: output path.
            train_no: training number
        '''
		print('Saving weights...')
		self.siamese.save_weights(path + '/output_%s/weights/siamese.h5' % train_no)
		self.emulator.save_weights(path + '/output_%s/weights/emulator.h5' % train_no)
		self.adversarial1.save_weights(path + '/output_%s/weights/adversarial1.h5' % train_no)
		#self.discriminator.save_weights(path + '/discriminator_weights')
		#self.adversarial2.save_weights(path + '/adversarial2_weights')
		#self.generator.save_weights(path + '/generator_weights')

	# Main function

	def train(self, train_steps=15000, batch_size=32, train_no=1, data_path=None, save_weights=True, output_path=None):

		''' Training stage.

        Args:
            train_steps: number of pre-training steps (iterations).
            batch_size: batch size.
            training number: training number.
            train_no: training number.
            save_weights: save model weights.
            output_path: output path.
        '''

		print('Training for ', train_steps)
		print('Training number = ', train_no)

		'''
		Training-stage
		'''
		# Number of tracks for the training + validation sample
		n_events = 1728000 + 100000
		n_train = n_events - 100000

		# Parameters for Gaussian noise
		mu = 0
		sigma = 1

		# import RICH simulation data

		print('Loading data...')

		kaon_data = pd.read_hdf(data_path)

		kaon_data = kaon_data.sample(n=n_events)
		kaon_data = kaon_data.reset_index(drop=True)

		kaon_data_train = kaon_data[:n_train]
		kaon_data_test = kaon_data[n_train:n_events]

		print("Producing training data...")

		# add all physics inputs

		P_kaon_data_train = kaon_data_train['TrackP']
		Pt_kaon_data_train = kaon_data_train['TrackPt']
		nTracks_kaon_data_train = kaon_data_train['NumLongTracks']
		numRich1_kaon_data_train = kaon_data_train['NumRich1Hits']
		numRich2_kaon_data_train = kaon_data_train['NumRich2Hits']
		rich1EntryX_kaon_data_train = kaon_data_train['TrackRich1EntryX']
		rich1EntryY_kaon_data_train = kaon_data_train['TrackRich1EntryY']
		rich1ExitX_kaon_data_train = kaon_data_train['TrackRich1ExitX']
		rich1ExitY_kaon_data_train = kaon_data_train['TrackRich1ExitY']
		rich2EntryX_kaon_data_train = kaon_data_train['TrackRich2EntryX']
		rich2EntryY_kaon_data_train = kaon_data_train['TrackRich2EntryY']
		rich2ExitX_kaon_data_train = kaon_data_train['TrackRich2ExitX']
		rich2ExitY_kaon_data_train = kaon_data_train['TrackRich2ExitY']

		# add different DLL outputs

		Dlle_kaon_data_train = kaon_data_train['RichDLLe2']
		Dlle2_kaon_data_train = kaon_data_train['RichDLLe3']
		Dllmu_kaon_data_train = kaon_data_train['RichDLLmu2']
		Dllmu2_kaon_data_train = kaon_data_train['RichDLLmu3']
		Dllk_kaon_data_train = kaon_data_train['RichDLLk2']
		Dllk2_kaon_data_train = kaon_data_train['RichDLLk3']
		Dllp_kaon_data_train = kaon_data_train['RichDLLp2']
		Dllp2_kaon_data_train = kaon_data_train['RichDLLp3']
		Dlld_kaon_data_train = kaon_data_train['RichDLLd2']
		Dlld2_kaon_data_train = kaon_data_train['RichDLLd3']
		Dllbt_kaon_data_train = kaon_data_train['RichDLLbt2']
		Dllbt2_kaon_data_train = kaon_data_train['RichDLLbt3']

		print("Producing plots of input and outputs...")

		# plot raw inputs/outputs

		plot_inputs(P_kaon_data_train, Pt_kaon_data_train, nTracks_kaon_data_train, numRich1_kaon_data_train, numRich2_kaon_data_train, rich1EntryX_kaon_data_train, rich1EntryY_kaon_data_train,
			rich1ExitX_kaon_data_train, rich1ExitY_kaon_data_train, rich2EntryX_kaon_data_train, rich2EntryY_kaon_data_train, rich2ExitX_kaon_data_train, rich2ExitY_kaon_data_train, norm_string='raw', train_no=train_no)

		#plot_inputs(P_kaon_data_train, Pt_kaon_data_train, nTracks_kaon_data_train, norm_string='raw', train_no=train_no)

		plot_outputs(Dlle_kaon_data_train, Dlle2_kaon_data_train, Dllmu_kaon_data_train, Dllmu2_kaon_data_train, Dllk_kaon_data_train, Dllk2_kaon_data_train, Dllp_kaon_data_train,
			Dllp2_kaon_data_train, Dlld_kaon_data_train, Dlld2_kaon_data_train, Dllbt_kaon_data_train, Dllbt2_kaon_data_train, norm_string='raw', train_no=train_no)

		# convert to numpy array
		
		P_kaon_data_train = P_kaon_data_train.to_numpy()
		Pt_kaon_data_train = Pt_kaon_data_train.to_numpy()
		nTracks_kaon_data_train = nTracks_kaon_data_train.to_numpy()
		numRich1_kaon_data_train = numRich1_kaon_data_train.to_numpy()
		numRich2_kaon_data_train = numRich2_kaon_data_train.to_numpy()
		rich1EntryX_kaon_data_train = rich1EntryX_kaon_data_train.to_numpy()
		rich1EntryY_kaon_data_train = rich1EntryY_kaon_data_train.to_numpy()
		rich1ExitX_kaon_data_train = rich1ExitX_kaon_data_train.to_numpy()
		rich1ExitY_kaon_data_train = rich1ExitY_kaon_data_train.to_numpy()
		rich2EntryX_kaon_data_train = rich2EntryX_kaon_data_train.to_numpy()
		rich2EntryY_kaon_data_train = rich2EntryY_kaon_data_train.to_numpy()
		rich2ExitX_kaon_data_train = rich2ExitX_kaon_data_train.to_numpy()
		rich2ExitY_kaon_data_train = rich2ExitY_kaon_data_train.to_numpy()

		Dlle_kaon_data_train = Dlle_kaon_data_train.to_numpy()
		Dlle2_kaon_data_train = Dlle2_kaon_data_train.to_numpy()
		Dllmu_kaon_data_train = Dllmu_kaon_data_train.to_numpy()
		Dllmu2_kaon_data_train = Dllmu2_kaon_data_train.to_numpy()
		Dllk_kaon_data_train = Dllk_kaon_data_train.to_numpy()
		Dllk2_kaon_data_train = Dllk2_kaon_data_train.to_numpy()
		Dllp_kaon_data_train = Dllp_kaon_data_train.to_numpy()
		Dllp2_kaon_data_train = Dllp2_kaon_data_train.to_numpy()
		Dlld_kaon_data_train = Dlld_kaon_data_train.to_numpy()
		Dlld2_kaon_data_train = Dlld2_kaon_data_train.to_numpy()
		Dllbt_kaon_data_train = Dllbt_kaon_data_train.to_numpy()
		Dllbt2_kaon_data_train = Dllbt2_kaon_data_train.to_numpy()

		# Reshape arrays

		P_kaon_data_train = np.array(P_kaon_data_train).reshape(-1, 1)
		Pt_kaon_data_train = np.array(Pt_kaon_data_train).reshape(-1, 1)
		nTracks_kaon_data_train = np.array(nTracks_kaon_data_train).reshape(-1, 1)
		numRich1_kaon_data_train = np.array(numRich1_kaon_data_train).reshape(-1, 1)
		numRich2_kaon_data_train = np.array(numRich2_kaon_data_train).reshape(-1, 1)
		rich1EntryX_kaon_data_train = np.array(rich1EntryX_kaon_data_train).reshape(-1, 1)
		rich1EntryY_kaon_data_train = np.array(rich1EntryY_kaon_data_train).reshape(-1, 1)
		rich1ExitX_kaon_data_train = np.array(rich1ExitX_kaon_data_train).reshape(-1, 1)
		rich1ExitY_kaon_data_train = np.array(rich1ExitY_kaon_data_train).reshape(-1, 1)
		rich2EntryX_kaon_data_train = np.array(rich2EntryX_kaon_data_train).reshape(-1, 1)
		rich2EntryY_kaon_data_train = np.array(rich2EntryY_kaon_data_train).reshape(-1, 1)
		rich2ExitX_kaon_data_train = np.array(rich2ExitX_kaon_data_train).reshape(-1, 1)
		rich2ExitY_kaon_data_train = np.array(rich2ExitY_kaon_data_train).reshape(-1, 1)

		Dlle_kaon_data_train = np.array(Dlle_kaon_data_train).reshape(-1, 1)
		Dlle2_kaon_data_train = np.array(Dlle2_kaon_data_train).reshape(-1, 1)
		Dllmu_kaon_data_train = np.array(Dllmu_kaon_data_train).reshape(-1, 1)
		Dllmu2_kaon_data_train = np.array(Dllmu2_kaon_data_train).reshape(-1, 1)
		Dllk_kaon_data_train = np.array(Dllk_kaon_data_train).reshape(-1, 1)
		Dllk2_kaon_data_train = np.array(Dllk2_kaon_data_train).reshape(-1, 1)
		Dllp_kaon_data_train = np.array(Dllp_kaon_data_train).reshape(-1, 1)
		Dllp2_kaon_data_train = np.array(Dllp2_kaon_data_train).reshape(-1, 1)
		Dlld_kaon_data_train = np.array(Dlld_kaon_data_train).reshape(-1, 1)
		Dlld2_kaon_data_train = np.array(Dlld2_kaon_data_train).reshape(-1, 1)
		Dllbt_kaon_data_train = np.array(Dllbt_kaon_data_train).reshape(-1, 1)
		Dllbt2_kaon_data_train = np.array(Dllbt2_kaon_data_train).reshape(-1, 1)

		inputs_kaon_data_train = np.concatenate((P_kaon_data_train, Pt_kaon_data_train, nTracks_kaon_data_train, numRich1_kaon_data_train, numRich2_kaon_data_train, rich1EntryX_kaon_data_train, 
		rich1EntryY_kaon_data_train, rich1ExitX_kaon_data_train, rich1ExitY_kaon_data_train, rich2EntryX_kaon_data_train, rich2EntryY_kaon_data_train, rich2ExitX_kaon_data_train, rich2ExitY_kaon_data_train), axis=1)
		Dll_kaon_data_train = np.concatenate((Dlle_kaon_data_train, Dllmu_kaon_data_train, Dllk_kaon_data_train, Dllp_kaon_data_train, Dlld_kaon_data_train, Dllbt_kaon_data_train), axis=1)
		Dll2_kaon_data_train = np.concatenate((Dlle2_kaon_data_train, Dllmu2_kaon_data_train, Dllk2_kaon_data_train, Dllp2_kaon_data_train, Dlld2_kaon_data_train, Dllbt2_kaon_data_train), axis=1)

		# Transform inputs/outputs

		print('Transforming inputs and outputs using Quantile Transformer...')

		scaler_inputs = QuantileTransformer(output_distribution='normal', n_quantiles=int(1e5), subsample=int(1e10)).fit(inputs_kaon_data_train)
		scaler_Dll = QuantileTransformer(output_distribution='normal', n_quantiles=int(1e5), subsample=int(1e10)).fit(Dll_kaon_data_train)
		scaler_Dll2 = QuantileTransformer(output_distribution='normal', n_quantiles=int(1e5), subsample=int(1e10)).fit(Dll2_kaon_data_train)

		#joblib.dump(scaler_inputs, '/home/iw273/rich-pid-fastsim-model-assisted-gan/model_kaon/scaler_inputs.save')
		#joblib.dump(scaler_Dll, '/home/iw273/rich-pid-fastsim-model-assisted-gan/model_kaon/scaler_Dll.save')
		
		inputs_kaon_data_train = scaler_inputs.transform(inputs_kaon_data_train)
		Dll_kaon_data_train = scaler_Dll.transform(Dll_kaon_data_train)
		Dll2_kaon_data_train = scaler_Dll2.transform(Dll2_kaon_data_train)

		# Map to reduced range

		inputs_kaon_data_train = mapping(inputs_kaon_data_train)
		Dll_kaon_data_train = mapping(Dll_kaon_data_train)
		Dll2_kaon_data_train = mapping(Dll2_kaon_data_train)

		print("Producing plots of normalised inputs and outputs...")
		
		# plot normalised inputs/outputs

		plots_inputs_kaon_data_train = np.hsplit(inputs_kaon_data_train, 13)
		plots_Dll_kaon_data_train = np.hsplit(Dll_kaon_data_train, self.observables)
		plots_Dll2_kaon_data_train = np.hsplit(Dll2_kaon_data_train, self.observables)

		P_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[0].flatten(), name='TrackP')
		Pt_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[1].flatten(), name='TrackPt')
		nTracks_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[2].flatten(), name='NumLongTracks')
		numRich1_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[3].flatten(), name='NumRich1Hits')
		numRich2_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[4].flatten(), name='NumRich2Hits')
		rich1EntryX_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[5].flatten(), name='TrackRich1EntryX')
		rich1EntryY_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[6].flatten(), name='TrackRich1EntryY')
		rich1ExitX_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[7].flatten(), name='TrackRich1ExitX')
		rich1ExitY_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[8].flatten(), name='TrackRich1ExitY')
		rich2EntryX_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[9].flatten(), name='TrackRich2EntryX')
		rich2EntryY_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[10].flatten(), name='TrackRich2EntryY')
		rich2ExitX_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[11].flatten(), name='TrackRich2ExitX')
		rich2ExitY_plots_inputs_kaon_data_train = pd.Series(data=plots_inputs_kaon_data_train[12].flatten(), name='TrackRich2ExitY')

		Dlle_plots_Dll_kaon_data_train = pd.Series(data=plots_Dll_kaon_data_train[0].flatten(), name='RichDLLe2')
		Dlle2_plots_Dll2_kaon_data_train = pd.Series(data=plots_Dll2_kaon_data_train[0].flatten(), name='RichDLLe3')
		Dllmu_plots_Dll_kaon_data_train = pd.Series(data=plots_Dll_kaon_data_train[1].flatten(), name='RichDLLmu2')
		Dllmu2_plots_Dll2_kaon_data_train = pd.Series(data=plots_Dll2_kaon_data_train[1].flatten(), name='RichDLLmu3')
		Dllk_plots_Dll_kaon_data_train = pd.Series(data=plots_Dll_kaon_data_train[2].flatten(), name='RichDLLk2')
		Dllk2_plots_Dll2_kaon_data_train = pd.Series(data=plots_Dll2_kaon_data_train[2].flatten(), name='RichDLLk3')
		Dllp_plots_Dll_kaon_data_train = pd.Series(data=plots_Dll_kaon_data_train[3].flatten(), name='RichDLL2')
		Dllp2_plots_Dll2_kaon_data_train = pd.Series(data=plots_Dll2_kaon_data_train[3].flatten(), name='RichDLLp3')
		Dlld_plots_Dll_kaon_data_train = pd.Series(data=plots_Dll_kaon_data_train[4].flatten(), name='RichDLLd2')
		Dlld2_plots_Dll2_kaon_data_train = pd.Series(data=plots_Dll2_kaon_data_train[4].flatten(), name='RichDLLd3')
		Dllbt_plots_Dll_kaon_data_train = pd.Series(data=plots_Dll_kaon_data_train[5].flatten(), name='RichDLLbt2')
		Dllbt2_plots_Dll2_kaon_data_train = pd.Series(data=plots_Dll2_kaon_data_train[5].flatten(), name='RichDLLbt3')

		plot_inputs(P_plots_inputs_kaon_data_train, Pt_plots_inputs_kaon_data_train, nTracks_plots_inputs_kaon_data_train, numRich1_plots_inputs_kaon_data_train, numRich2_plots_inputs_kaon_data_train,
		rich1EntryX_plots_inputs_kaon_data_train, rich1EntryY_plots_inputs_kaon_data_train, rich1ExitX_plots_inputs_kaon_data_train, rich1ExitY_plots_inputs_kaon_data_train, rich2EntryX_plots_inputs_kaon_data_train,
		rich2EntryY_plots_inputs_kaon_data_train, rich2ExitX_plots_inputs_kaon_data_train, rich2ExitY_plots_inputs_kaon_data_train, norm_string='norm', train_no=train_no)

		#plot_inputs(P_plots_inputs_kaon_data_train, Pt_plots_inputs_kaon_data_train, nTracks_plots_inputs_kaon_data_train, norm_string='norm', train_no=train_no)

		plot_outputs(Dlle_plots_Dll_kaon_data_train, Dlle2_plots_Dll2_kaon_data_train, Dllmu_plots_Dll_kaon_data_train, Dllmu2_plots_Dll2_kaon_data_train, Dllk_plots_Dll_kaon_data_train,
		Dllk2_plots_Dll2_kaon_data_train, Dllp_plots_Dll_kaon_data_train, Dllp2_plots_Dll2_kaon_data_train, Dlld_plots_Dll_kaon_data_train, Dlld2_plots_Dll2_kaon_data_train, Dllbt_plots_Dll_kaon_data_train,
		Dllbt2_plots_Dll2_kaon_data_train, norm_string='norm', train_no=train_no)
		
		# REPEATING FOR TESTING DATA

		print("Producing testing data...")

		# add all physics inputs

		P_kaon_data_test = kaon_data_test['TrackP']
		Pt_kaon_data_test = kaon_data_test['TrackPt']
		nTracks_kaon_data_test = kaon_data_test['NumLongTracks']
		numRich1_kaon_data_test = kaon_data_test['NumRich1Hits']
		numRich2_kaon_data_test = kaon_data_test['NumRich2Hits']
		rich1EntryX_kaon_data_test = kaon_data_test['TrackRich1EntryX']
		rich1EntryY_kaon_data_test = kaon_data_test['TrackRich1EntryY']
		rich1ExitX_kaon_data_test = kaon_data_test['TrackRich1ExitX']
		rich1ExitY_kaon_data_test = kaon_data_test['TrackRich1ExitY']
		rich2EntryX_kaon_data_test = kaon_data_test['TrackRich2EntryX']
		rich2EntryY_kaon_data_test = kaon_data_test['TrackRich2EntryY']
		rich2ExitX_kaon_data_test = kaon_data_test['TrackRich2ExitX']
		rich2ExitY_kaon_data_test = kaon_data_test['TrackRich2ExitY']

		# add different DLL outputs

		Dlle_kaon_data_test = kaon_data_test['RichDLLe2']
		Dlle2_kaon_data_test = kaon_data_test['RichDLLe3']
		Dllmu_kaon_data_test = kaon_data_test['RichDLLmu2']
		Dllmu2_kaon_data_test = kaon_data_test['RichDLLmu3']
		Dllk_kaon_data_test = kaon_data_test['RichDLLk2']
		Dllk2_kaon_data_test = kaon_data_test['RichDLLk3']
		Dllp_kaon_data_test = kaon_data_test['RichDLLp2']
		Dllp2_kaon_data_test = kaon_data_test['RichDLLp3']
		Dlld_kaon_data_test = kaon_data_test['RichDLLd2']
		Dlld2_kaon_data_test = kaon_data_test['RichDLLd3']
		Dllbt_kaon_data_test = kaon_data_test['RichDLLbt2']
		Dllbt2_kaon_data_test = kaon_data_test['RichDLLbt3']

		# convert to numpy array

		P_kaon_data_test = P_kaon_data_test.to_numpy()
		Pt_kaon_data_test = Pt_kaon_data_test.to_numpy()
		nTracks_kaon_data_test = nTracks_kaon_data_test.to_numpy()
		numRich1_kaon_data_test = numRich1_kaon_data_test.to_numpy()
		numRich2_kaon_data_test = numRich2_kaon_data_test.to_numpy()
		rich1EntryX_kaon_data_test = rich1EntryX_kaon_data_test.to_numpy()
		rich1EntryY_kaon_data_test = rich1EntryY_kaon_data_test.to_numpy()
		rich1ExitX_kaon_data_test = rich1ExitX_kaon_data_test.to_numpy()
		rich1ExitY_kaon_data_test = rich1ExitY_kaon_data_test.to_numpy()
		rich2EntryX_kaon_data_test = rich2EntryX_kaon_data_test.to_numpy()
		rich2EntryY_kaon_data_test = rich2EntryY_kaon_data_test.to_numpy()
		rich2ExitX_kaon_data_test = rich2ExitX_kaon_data_test.to_numpy()
		rich2ExitY_kaon_data_test = rich2ExitY_kaon_data_test.to_numpy()
		Dlle_kaon_data_test = Dlle_kaon_data_test.to_numpy()
		Dlle2_kaon_data_test = Dlle2_kaon_data_test.to_numpy()
		Dllmu_kaon_data_test = Dllmu_kaon_data_test.to_numpy()
		Dllmu2_kaon_data_test = Dllmu2_kaon_data_test.to_numpy()
		Dllk_kaon_data_test = Dllk_kaon_data_test.to_numpy()
		Dllk2_kaon_data_test = Dllk2_kaon_data_test.to_numpy()
		Dllp_kaon_data_test = Dllp_kaon_data_test.to_numpy()
		Dllp2_kaon_data_test = Dllp2_kaon_data_test.to_numpy()
		Dlld_kaon_data_test = Dlld_kaon_data_test.to_numpy()
		Dlld2_kaon_data_test = Dlld2_kaon_data_test.to_numpy()
		Dllbt_kaon_data_test = Dllbt_kaon_data_test.to_numpy()
		Dllbt2_kaon_data_test = Dllbt2_kaon_data_test.to_numpy()

		P_kaon_data_test = np.array(P_kaon_data_test).reshape(-1, 1)
		Pt_kaon_data_test = np.array(Pt_kaon_data_test).reshape(-1, 1)
		nTracks_kaon_data_test = np.array(nTracks_kaon_data_test).reshape(-1, 1)
		numRich1_kaon_data_test = np.array(numRich1_kaon_data_test).reshape(-1, 1)
		numRich2_kaon_data_test = np.array(numRich2_kaon_data_test).reshape(-1, 1)
		rich1EntryX_kaon_data_test = np.array(rich1EntryX_kaon_data_test).reshape(-1, 1)
		rich1EntryY_kaon_data_test = np.array(rich1EntryY_kaon_data_test).reshape(-1, 1)
		rich1ExitX_kaon_data_test = np.array(rich1ExitX_kaon_data_test).reshape(-1, 1)
		rich1ExitY_kaon_data_test = np.array(rich1ExitY_kaon_data_test).reshape(-1, 1)
		rich2EntryX_kaon_data_test = np.array(rich2EntryX_kaon_data_test).reshape(-1, 1)
		rich2EntryY_kaon_data_test = np.array(rich2EntryY_kaon_data_test).reshape(-1, 1)
		rich2ExitX_kaon_data_test = np.array(rich2ExitX_kaon_data_test).reshape(-1, 1)
		rich2ExitY_kaon_data_test = np.array(rich2ExitY_kaon_data_test).reshape(-1, 1)
		Dlle_kaon_data_test = np.array(Dlle_kaon_data_test).reshape(-1, 1)
		Dlle2_kaon_data_test = np.array(Dlle2_kaon_data_test).reshape(-1, 1)
		Dllmu_kaon_data_test = np.array(Dllmu_kaon_data_test).reshape(-1, 1)
		Dllmu2_kaon_data_test = np.array(Dllmu2_kaon_data_test).reshape(-1, 1)
		Dllk_kaon_data_test = np.array(Dllk_kaon_data_test).reshape(-1, 1)
		Dllk2_kaon_data_test = np.array(Dllk2_kaon_data_test).reshape(-1, 1)
		Dllp_kaon_data_test = np.array(Dllp_kaon_data_test).reshape(-1, 1)
		Dllp2_kaon_data_test = np.array(Dllp2_kaon_data_test).reshape(-1, 1)
		Dlld_kaon_data_test = np.array(Dlld_kaon_data_test).reshape(-1, 1)
		Dlld2_kaon_data_test = np.array(Dlld2_kaon_data_test).reshape(-1, 1)
		Dllbt_kaon_data_test = np.array(Dllbt_kaon_data_test).reshape(-1, 1)
		Dllbt2_kaon_data_test = np.array(Dllbt2_kaon_data_test).reshape(-1, 1)

		inputs_kaon_data_test = np.concatenate((P_kaon_data_test, Pt_kaon_data_test, nTracks_kaon_data_test, numRich1_kaon_data_test, numRich2_kaon_data_test, rich1EntryX_kaon_data_test, rich1EntryY_kaon_data_test, rich1ExitX_kaon_data_test, rich1ExitY_kaon_data_test, rich2EntryX_kaon_data_test, rich2EntryY_kaon_data_test, rich2ExitX_kaon_data_test, rich2ExitY_kaon_data_test), axis=1)
		Dll_kaon_data_test = np.concatenate((Dlle_kaon_data_test, Dllmu_kaon_data_test, Dllk_kaon_data_test, Dllp_kaon_data_test, Dlld_kaon_data_test, Dllbt_kaon_data_test), axis=1)
		Dll2_kaon_data_test = np.concatenate((Dlle2_kaon_data_test, Dllmu2_kaon_data_test, Dllk2_kaon_data_test, Dllp2_kaon_data_test, Dlld2_kaon_data_test, Dllbt2_kaon_data_test), axis=1)

		# Transform inputs/outputs

		print('Transforming inputs and outputs using Quantile Transformer...')
		
		inputs_kaon_data_test = scaler_inputs.transform(inputs_kaon_data_test)
		Dll_kaon_data_test = scaler_Dll.transform(Dll_kaon_data_test)
		Dll2_kaon_data_test = scaler_Dll2.transform(Dll2_kaon_data_test)

		# Map to reduced range

		inputs_kaon_data_test = mapping(inputs_kaon_data_test)
		Dll_kaon_data_test = mapping(Dll_kaon_data_test)
		Dll2_kaon_data_test = mapping(Dll2_kaon_data_test)

		# Producing testing data
		params_list_test = np.random.normal(loc=mu, scale=sigma, size=[len(kaon_data_test), self.params])
		for e in range(len(kaon_data_test)):
			params_list_test[e][0] = inputs_kaon_data_test[e][0]
			params_list_test[e][1] = inputs_kaon_data_test[e][1]
			params_list_test[e][2] = inputs_kaon_data_test[e][2]
			params_list_test[e][3] = inputs_kaon_data_test[e][3]
			params_list_test[e][4] = inputs_kaon_data_test[e][4]
			params_list_test[e][5] = inputs_kaon_data_test[e][5]
			params_list_test[e][6] = inputs_kaon_data_test[e][6]
			params_list_test[e][7] = inputs_kaon_data_test[e][7]
			params_list_test[e][8] = inputs_kaon_data_test[e][8]
			params_list_test[e][9] = inputs_kaon_data_test[e][9]
			params_list_test[e][10] = inputs_kaon_data_test[e][10]
			params_list_test[e][11] = inputs_kaon_data_test[e][11]
			params_list_test[e][12] = inputs_kaon_data_test[e][12]

		obs_simu_1_test = np.zeros((len(kaon_data_test), self.observables, 1))
		obs_simu_1_test.fill(-1)
		for e in range(len(kaon_data_test)):
			obs_simu_1_test[e][0][0] = Dll_kaon_data_test[e][0]
			obs_simu_1_test[e][1][0] = Dll_kaon_data_test[e][1]
			obs_simu_1_test[e][2][0] = Dll_kaon_data_test[e][2]
			obs_simu_1_test[e][3][0] = Dll_kaon_data_test[e][3]
			obs_simu_1_test[e][4][0] = Dll_kaon_data_test[e][4]
			obs_simu_1_test[e][5][0] = Dll_kaon_data_test[e][5]

		obs_simu_2_test = np.zeros((len(kaon_data_test), self.observables, 1))
		obs_simu_2_test.fill(-1)
		for e in range(len(kaon_data_test)):
			obs_simu_2_test[e][0][0] = Dll2_kaon_data_test[e][0]
			obs_simu_2_test[e][1][0] = Dll2_kaon_data_test[e][1]
			obs_simu_2_test[e][2][0] = Dll2_kaon_data_test[e][2]
			obs_simu_2_test[e][3][0] = Dll2_kaon_data_test[e][3]
			obs_simu_2_test[e][4][0] = Dll2_kaon_data_test[e][4]
			obs_simu_2_test[e][5][0] = Dll2_kaon_data_test[e][5]

		event_no_par = 0
		event_no_obs_1 = 0
		event_no_obs_2 = 0

		d_hist, g_hist = list(), list()

		print('Beginning training...')
		'''
		#Pre-training stage
		'''
		for train_step in range(train_steps):
			log_mesg = '%d' % train_step
			noise_value = 0.6
			params_list = np.random.normal(loc=mu,scale=sigma, size=[batch_size, self.params])
			y_ones = np.ones([batch_size, 1])
			y_zeros = np.zeros([batch_size, 1])

			# add physics parameters + noise to params_list

			for b in range(batch_size):
				params_list[b][0] = inputs_kaon_data_train[event_no_par][0]
				params_list[b][1] = inputs_kaon_data_train[event_no_par][1]
				params_list[b][2] = inputs_kaon_data_train[event_no_par][2]
				params_list[b][3] = inputs_kaon_data_train[event_no_par][3]
				params_list[b][4] = inputs_kaon_data_train[event_no_par][4]
				params_list[b][5] = inputs_kaon_data_train[event_no_par][5]
				params_list[b][6] = inputs_kaon_data_train[event_no_par][6]
				params_list[b][7] = inputs_kaon_data_train[event_no_par][7]
				params_list[b][8] = inputs_kaon_data_train[event_no_par][8]
				params_list[b][9] = inputs_kaon_data_train[event_no_par][9]
				params_list[b][10] = inputs_kaon_data_train[event_no_par][10]
				params_list[b][11] = inputs_kaon_data_train[event_no_par][11]
				params_list[b][12] = inputs_kaon_data_train[event_no_par][12]
				event_no_par += 1

			# Step 1
			# simulated observables (number 1)
			obs_simu_1 = np.zeros((batch_size, self.observables, 1))
			obs_simu_1.fill(-1)
			for b in range(batch_size):
				obs_simu_1[b][0][0] = Dll_kaon_data_train[event_no_obs_1][0]
				obs_simu_1[b][1][0] = Dll_kaon_data_train[event_no_obs_1][1]
				obs_simu_1[b][2][0] = Dll_kaon_data_train[event_no_obs_1][2]
				obs_simu_1[b][3][0] = Dll_kaon_data_train[event_no_obs_1][3]
				obs_simu_1[b][4][0] = Dll_kaon_data_train[event_no_obs_1][4]
				obs_simu_1[b][5][0] = Dll_kaon_data_train[event_no_obs_1][5]
				event_no_obs_1 += 1

			obs_simu_1_copy = np.copy(obs_simu_1)

			# simulated observables (Gaussian smeared - number 2)
			obs_simu_2 = np.zeros((batch_size, self.observables, 1))
			obs_simu_2.fill(-1)
			for b in range(batch_size):
				obs_simu_2[b][0][0] = Dll2_kaon_data_train[event_no_obs_2][0]
				obs_simu_2[b][1][0] = Dll2_kaon_data_train[event_no_obs_2][1]
				obs_simu_2[b][2][0] = Dll2_kaon_data_train[event_no_obs_2][2]
				obs_simu_2[b][3][0] = Dll2_kaon_data_train[event_no_obs_2][3]
				obs_simu_2[b][4][0] = Dll2_kaon_data_train[event_no_obs_2][4]
				obs_simu_2[b][5][0] = Dll2_kaon_data_train[event_no_obs_2][5]
				event_no_obs_2 += 1

			obs_simu_2_copy = np.copy(obs_simu_2)

			# emulated DLL values
			obs_emul = self.emulator.predict(params_list)
			obs_emul_copy = np.copy(obs_emul)

			# decay the learn rate
			if(train_step % 1000 == 0 and train_step>0):
				decay_factor = 0.7
				siamese_lr = K.eval(self.siamese.optimizer.lr)
				K.set_value(self.siamese.optimizer.lr, siamese_lr*decay_factor)
				print('lr for Siamese network updated from %f to %f' % (siamese_lr, siamese_lr*decay_factor))
				adversarial1_lr = K.eval(self.adversarial1.optimizer.lr)
				K.set_value(self.adversarial1.optimizer.lr, adversarial1_lr*decay_factor)
				print('lr for Adversarial1 network updated from %f to %f' % (adversarial1_lr, adversarial1_lr*decay_factor))

			loss_simu_list = [obs_simu_1_copy, obs_simu_2_copy]
			loss_fake_list = [obs_simu_1_copy, obs_emul_copy]

			input_val = 0
			# swap which inputs to give to Siamese network
			if(np.random.random() < 0.5):
				loss_simu_list[0], loss_simu_list[1] = loss_simu_list[1], loss_simu_list[0]

			if(np.random.random() < 0.5):
				loss_fake_list[0] = obs_simu_2_copy
				input_val = 1

			# Add noise
			y_ones = np.array([np.random.uniform(0.95, 1.00) for x in range(batch_size)]).reshape([batch_size, 1])
			y_zeros = np.array([np.random.uniform(0.00, 0.05) for x in range(batch_size)]).reshape([batch_size, 1])

			if(input_val == 0):
				if np.random.random() < noise_value:
					for b in range(batch_size):
						if np.random.random() < noise_value:
							obs_simu_1_copy[b], obs_simu_2_copy[b] = obs_simu_2[b], obs_simu_1[b]
							obs_simu_1_copy[b], obs_emul_copy[b] = obs_emul[b], obs_simu_1[b]
			
			if(input_val == 1):
				if np.random.random() < noise_value:
					for b in range(batch_size):
						if np.random.random() < noise_value:
							obs_simu_1_copy[b], obs_simu_2_copy[b] = obs_simu_2[b], obs_simu_1[b]
							obs_simu_2_copy[b], obs_emul_copy[b] = obs_emul[b], obs_simu_2[b]

			# train siamese
			d_loss_simu = self.siamese.train_on_batch(loss_simu_list, y_ones)
			d_loss_fake = self.siamese.train_on_batch(loss_fake_list, y_zeros)
			d_loss = 0.5 * np.add(d_loss_simu, d_loss_fake)
			log_mesg = '%s [S loss: %f]' % (log_mesg, d_loss[0])

			#print(log_mesg)
			#print('--------------------')

			#noise_value*=0.999
			
			#Step 2
			
			# Add noise
			for b in range(batch_size):
				if np.random.random() < noise_value:
					y_ones[b,0] = np.random.uniform(0.0, 0.01)

			# train emulator
			a_loss_list = [obs_simu_1, params_list]

			# performance something similar with this
			if(np.random.random() < 0.5):
				a_loss_list[0] = obs_simu_2
			
			a_loss = self.adversarial1.train_on_batch(a_loss_list, y_ones)
			log_mesg = '%s [E loss: %f]' % (log_mesg, a_loss[0])
			print(log_mesg)
			print('--------------------')

			noise_value*=0.999
			"""
			
			if(train_step % (25) == 0 and train_step>0):

				obs_simu_1_split = np.hsplit(obs_simu_1, 6)
				obs_simu_2_split = np.hsplit(obs_simu_2, 6)
				obs_emul_split = np.hsplit(obs_emul, 6)

				# Get the observables back from the emulator
				
				print('DLLe:')
				print('Simulator (True) value = ',  obs_simu_1_split[0][0][0][0])
				print('Simulator (Smeared) value = ',  obs_simu_2_split[0][0][0][0])
				print('Emulator value = ', obs_emul_split[0][0][0][0])
				print('DLLmu:')
				print('Simulator (True) value = ',  obs_simu_1_split[1][0][0][0])
				print('Simulator (Smeared) value = ',  obs_simu_2_split[1][0][0][0])
				print('Emulator value = ', obs_emul_split[1][0][0][0])
				print('DLLk:')
				print('Simulator (True) value = ',  obs_simu_1_split[2][0][0][0])
				print('Simulator (Smeared) value = ',  obs_simu_2_split[2][0][0][0])
				print('Emulator value = ', obs_emul_split[2][0][0][0])
				print('DLLp:')
				print('Simulator (True) value = ',  obs_simu_1_split[3][0][0][0])
				print('Simulator (Smeared) value = ',  obs_simu_2_split[3][0][0][0])
				print('Emulator value = ', obs_emul_split[3][0][0][0])
				print('DLLd:')
				print('Simulator (True) value = ',  obs_simu_1_split[4][0][0][0])
				print('Simulator (Smeared) value = ',  obs_simu_2_split[4][0][0][0])
				print('Emulator value = ', obs_emul_split[4][0][0][0])
				print('DLLbt:')
				print('Simulator (True) value = ',  obs_simu_1_split[5][0][0][0])
				print('Simulator (Smeared) value = ',  obs_simu_2_split[5][0][0][0])
				print('Emulator value = ', obs_emul_split[5][0][0][0])
				print('--------------------')

			"""
			if(train_step % (1000) == 0 and train_step>0):

				# produce histograms of emulated - true(smeared) and plot these every 2700 iterations

				print('PRODUCING PLOTS')

				if not os.path.exists(output_path +'/output_%s/iteration/train_step_%s/' % (train_no, train_step)):
					os.makedirs(output_path + '/output_%s/iteration/train_step_%s/' % (train_no, train_step))

				# predict parameter values

				obs_emul_test = self.emulator.predict(params_list_test)

				# produce correlation and target plots (normalised and raw)

				plot_obs_emul_test = obs_emul_test.reshape(obs_emul_test.shape[0], obs_emul_test.shape[1])
				plot_obs_simu_1_test = obs_simu_1_test.reshape(obs_simu_1_test.shape[0], obs_simu_1_test.shape[1])
				plot_obs_simu_2_test = obs_simu_2_test.reshape(obs_simu_2_test.shape[0], obs_simu_2_test.shape[1])

				# normalised plots

				plot_obs_emul_test_split = np.hsplit(plot_obs_emul_test, 6)
				plot_obs_simu_1_test_split = np.hsplit(plot_obs_simu_1_test, 6)
				plot_obs_simu_2_test_split = np.hsplit(plot_obs_simu_2_test, 6)

				# raw plots

				plot_obs_emul_test_raw = antimapping(plot_obs_emul_test)
				plot_obs_simu_1_test_raw = antimapping(plot_obs_simu_1_test)
				plot_obs_simu_2_test_raw = antimapping(plot_obs_simu_2_test)

				plot_obs_emul_test_raw = scaler_Dll.inverse_transform(plot_obs_emul_test_raw)
				plot_obs_simu_1_test_raw = scaler_Dll.inverse_transform(plot_obs_simu_1_test_raw)
				plot_obs_simu_2_test_raw = scaler_Dll2.inverse_transform(plot_obs_simu_2_test_raw)

				plot_obs_emul_test_raw_split = np.hsplit(plot_obs_emul_test_raw, 6)
				plot_obs_simu_1_test_raw_split = np.hsplit(plot_obs_simu_1_test_raw, 6)
				plot_obs_simu_2_test_raw_split = np.hsplit(plot_obs_simu_2_test_raw, 6)

				if(train_step == 15000):
					print("Overlaps:")
					print("DLLe = ", histogram_intersection(plot_obs_simu_1_test_raw_split[0].flatten(), plot_obs_emul_test_raw_split[0].flatten(), 750, [-80,20]))
					print("DLLmu = ", histogram_intersection(plot_obs_simu_1_test_raw_split[1].flatten(), plot_obs_emul_test_raw_split[1].flatten(), 750, [-50,20])) 
					print("DLLk = ", histogram_intersection(plot_obs_simu_1_test_raw_split[2].flatten(), plot_obs_emul_test_raw_split[2].flatten(), 750, [-60, 80])) 
					print("DLLp = ", histogram_intersection(plot_obs_simu_1_test_raw_split[3].flatten(), plot_obs_emul_test_raw_split[3].flatten(), 750, [-60, 60])) 
					print("DLLd = ", histogram_intersection(plot_obs_simu_1_test_raw_split[4].flatten(), plot_obs_emul_test_raw_split[4].flatten(), 750, [-60,60])) 
					print("DLLbt = ", histogram_intersection(plot_obs_simu_1_test_raw_split[5].flatten(), plot_obs_emul_test_raw_split[5].flatten(), 750, [-60,60]))  

				# produce true correlation plots
				plot_correlation(plot_obs_simu_1_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), plot_obs_simu_1_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(),
					plot_obs_simu_1_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), plot_obs_simu_1_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 
					plot_obs_simu_1_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), plot_obs_simu_1_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), train_step, 'true', train_no)

				# produce smeared correlation plots
				plot_correlation(plot_obs_simu_2_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), plot_obs_simu_2_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(),
					plot_obs_simu_2_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), plot_obs_simu_2_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 
					plot_obs_simu_2_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), plot_obs_simu_2_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), train_step, 'smeared', train_no)

				# produce emulated and simulated histograms superimposed (normalised)
				plot_DLL(plot_obs_simu_1_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), plot_obs_simu_1_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(),
					plot_obs_simu_1_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), plot_obs_simu_1_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 
					plot_obs_simu_1_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), plot_obs_simu_1_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), train_step, 'true', train_no, 'norm')

				# produce smeared emulated and simulated histograms superimposed (normalised)
				plot_DLL(plot_obs_simu_2_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), plot_obs_simu_2_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(),
					plot_obs_simu_2_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), plot_obs_simu_2_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 
					plot_obs_simu_2_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), plot_obs_simu_2_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), train_step, 'smeared', train_no, 'norm')

				# produce emulated and simulated histograms superimposed (raw)
				plot_DLL(plot_obs_simu_1_test_raw_split[0].flatten(), plot_obs_emul_test_raw_split[0].flatten(), plot_obs_simu_1_test_raw_split[1].flatten(), plot_obs_emul_test_raw_split[1].flatten(),
					plot_obs_simu_1_test_raw_split[2].flatten(), plot_obs_emul_test_raw_split[2].flatten(), plot_obs_simu_1_test_raw_split[3].flatten(), plot_obs_emul_test_raw_split[3].flatten(), 
					plot_obs_simu_1_test_raw_split[4].flatten(), plot_obs_emul_test_raw_split[4].flatten(), plot_obs_simu_1_test_raw_split[5].flatten(), plot_obs_emul_test_raw_split[5].flatten(), train_step, 'true', train_no, 'raw')

				# produce smeared emulated and simulated histograms superimposed (raw)
				plot_DLL(plot_obs_simu_2_test_raw_split[0].flatten(), plot_obs_emul_test_raw_split[0].flatten(), plot_obs_simu_2_test_raw_split[1].flatten(), plot_obs_emul_test_raw_split[1].flatten(),
					plot_obs_simu_2_test_raw_split[2].flatten(), plot_obs_emul_test_raw_split[2].flatten(), plot_obs_simu_2_test_raw_split[3].flatten(), plot_obs_emul_test_raw_split[3].flatten(), 
					plot_obs_simu_2_test_raw_split[4].flatten(), plot_obs_emul_test_raw_split[4].flatten(), plot_obs_simu_2_test_raw_split[5].flatten(), plot_obs_emul_test_raw_split[5].flatten(), train_step, 'smeared', train_no, 'raw')

				# calculate deltsDLLs and plot
					
				simu_1_diff = obs_emul_test - obs_simu_1_test
				simu_2_diff = obs_emul_test - obs_simu_2_test

				simu_1_diff = simu_1_diff.reshape(obs_emul_test.shape[0], obs_emul_test.shape[1])
				simu_2_diff = simu_2_diff.reshape(obs_emul_test.shape[0], obs_emul_test.shape[1])

				simu_1_diff_split = np.hsplit(simu_1_diff, 6)
				simu_2_diff_split = np.hsplit(simu_2_diff, 6)
				
				Dlle_simu_1_diff = pd.Series(data=simu_1_diff_split[0].flatten(), name='RichDLLe2')
				Dllmu_simu_1_diff = pd.Series(data=simu_1_diff_split[1].flatten(), name='RichDLLmu2')
				Dllk_simu_1_diff = pd.Series(data=simu_1_diff_split[2].flatten(), name='RichDLLk2')
				Dllp_simu_1_diff = pd.Series(data=simu_1_diff_split[3].flatten(), name='RichDLLp2')
				Dlld_simu_1_diff = pd.Series(data=simu_1_diff_split[4].flatten(), name='RichDLLd2')
				Dllbt_simu_1_diff = pd.Series(data=simu_1_diff_split[5].flatten(), name='RichDLLbt2')

				Dlle_simu_2_diff = pd.Series(data=simu_2_diff_split[0].flatten(), name='RichDLLe3')
				Dllmu_simu_2_diff = pd.Series(data=simu_2_diff_split[1].flatten(), name='RichDLLmu3')
				Dllk_simu_2_diff = pd.Series(data=simu_2_diff_split[2].flatten(), name='RichDLLk3')
				Dllp_simu_2_diff = pd.Series(data=simu_2_diff_split[3].flatten(), name='RichDLLp3')
				Dlld_simu_2_diff = pd.Series(data=simu_2_diff_split[4].flatten(), name='RichDLLd3')
				Dllbt_simu_2_diff = pd.Series(data=simu_2_diff_split[5].flatten(), name='RichDLLbt3')

				plot_deltaDLL(Dlle_simu_1_diff, Dllmu_simu_1_diff, Dllk_simu_1_diff, Dllp_simu_1_diff, Dlld_simu_1_diff, Dllbt_simu_1_diff, train_step, 'true', train_no)
				plot_deltaDLL(Dlle_simu_2_diff, Dllmu_simu_2_diff, Dllk_simu_2_diff, Dllp_simu_2_diff, Dlld_simu_2_diff, Dllbt_simu_2_diff, train_step, 'smeared', train_no)			
				
			# record history
			d_hist.append(d_loss[0])
			g_hist.append(a_loss[0])

			if(train_step % (1000) == 0 and train_step>0):
				# summarize history for loss every 1000 iterations
				plot_history(d_hist, g_hist, train_no)

		# save weights

		if save_weights:
			self.save_weights(output_path, train_no)

		
		exit(0)
		
if __name__ == '__main__':

	magan = ModelAssistedGANPID(params=args.params_physics + args.params_noise, observables=args.observables, print_summary=args.print_summary)

	if args.train:
		magan.train(train_steps=args.train_steps, batch_size=args.batch_size, train_no=args.train_no, data_path=args.data_path, save_weights=args.save_weights, output_path=args.output_path)

