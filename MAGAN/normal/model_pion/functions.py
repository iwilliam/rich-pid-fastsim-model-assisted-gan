import matplotlib.pyplot as plt
import numpy as np

def plot_inputs(*args, norm_string, train_no):

	l = []

	for arg in args:
		l.append(arg)

	quotient, remainder = divmod(len(l), 3)
	
	if(remainder != 0):
		quotient += 1

	fig, ax =  plt.subplots(quotient, 3)
	
	arg_no = 0
	for i in range(quotient):
		for j in range(3):
			try:
				l[arg_no].hist(bins=50, ax=ax[i,j], figsize=(25, 25))
				ax[i,j].set_title(l[arg_no].name)
			except IndexError:
				print("Skipping...")
			arg_no += 1			
	
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/outputs/output_%s/plot_inputs_%s.pdf' % (train_no, norm_string) )
	plt.close()

def plot_outputs(*args, norm_string, train_no):

	l = []

	for arg in args:
		l.append(arg)

	quotient, remainder = divmod(len(l), 3)
	
	if(remainder != 0):
		quotient += 1

	fig, ax =  plt.subplots(quotient, 3)
	
	arg_no = 0
	for i in range(quotient):
		for j in range(3):
			try:
				l[arg_no].hist(bins=50, ax=ax[i,j], figsize=(25, 25))
				ax[i,j].set_title(l[arg_no].name)
			except IndexError:
				print("Skipping...")
			arg_no += 1			
	
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/outputs/output_%s/plot_outputs_%s.pdf' % (train_no, norm_string) )
	plt.close()


def plot_history_full(d1_hist, d2_hist, d_hist, g_hist, a1_hist, a2_hist):
	# plot loss
	plt.subplot(2, 1, 1)
	plt.plot(d1_hist, label='S-real')
	plt.plot(d2_hist, label='S-fake')
	plt.plot(d_hist, label='S')
	plt.plot(g_hist, label='A')
	plt.title('Model losses (Top)/Discriminator Accuracy (Bottom)')
	plt.ylabel('Loss')
	plt.xlabel('Batch')
	plt.legend()
	# plot discriminator accuracy
	plt.subplot(2, 1, 2)
	plt.ylabel('Acc')
	plt.xlabel('Batch')
	plt.plot(a1_hist, label='Acc-real')
	plt.plot(a2_hist, label='Acc-fake')
	plt.legend()

	# save plot to file
	#plt.savefig('results/plot_loss_acc.pdf')
	plt.close()

def plot_history(d_hist, g_hist, train_no):
	# plot loss
	plt.subplot(2, 1, 1)
	plt.plot(d_hist, label='S', linestyle='dashed', linewidth='1.0')
	plt.plot(g_hist, label='A', linestyle='dashed', linewidth='1.0')
	plt.title('Model losses (Top)')
	plt.ylabel('Loss')
	plt.xlabel('Batch')
	plt.legend()

	# save plot to file
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/outputs/output_%s/plot_loss.pdf' % train_no)
	plt.close()

def plot_correlation(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt, train_step, simu_string, train_no):
	
	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	ax[0,0].hist2d(obs_emul_e, obs_simu_e, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[0,0].set_title('RichDLLe')
	ax[0,0].set_ylabel('Simulated')
	ax[0,0].set_xlabel('Emulated')
	ax[0,0].set_ylim(-0.75, 0.75)
	ax[0,0].set_xlim(-0.75, 0.75)
	ax[0,1].hist2d(obs_emul_mu, obs_simu_mu, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[0,1].set_title('RichDLLmu')
	ax[0,1].set_ylabel('Simulated')
	ax[0,1].set_xlabel('Emulated')
	ax[0,1].set_ylim(-0.75, 0.75)
	ax[0,1].set_xlim(-0.75, 0.75)
	ax[1,0].hist2d(obs_emul_k, obs_simu_k, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[1,0].set_title('RichDLLk')
	ax[1,0].set_ylabel('Simulated')
	ax[1,0].set_xlabel('Emulated')
	ax[1,0].set_ylim(-0.75, 0.75)
	ax[1,0].set_xlim(-0.75, 0.75)
	ax[1,1].hist2d(obs_emul_p, obs_simu_p, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[1,1].set_title('RichDLLp')
	ax[1,1].set_ylabel('Simulated')
	ax[1,1].set_xlabel('Emulated')
	ax[1,1].set_ylim(-0.75, 0.75)
	ax[1,1].set_xlim(-0.75, 0.75)
	ax[2,0].hist2d(obs_emul_d, obs_simu_d,  bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[2,0].set_title('RichDLLd')
	ax[2,0].set_ylabel('Simulated')
	ax[2,0].set_xlabel('Emulated')
	ax[2,0].set_ylim(-0.75, 0.75)
	ax[2,0].set_xlim(-0.75, 0.75)
	ax[2,1].hist2d(obs_emul_bt, obs_simu_bt, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[2,1].set_title('RichDLLbt')
	ax[2,1].set_ylabel('Simulated')
	ax[2,1].set_xlabel('Emulated')
	ax[2,1].set_ylim(-0.75, 0.75)
	ax[2,1].set_xlim(-0.75, 0.75)

	plt.tight_layout()
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/outputs/output_%s/iteration/train_step_%s/plot_correlation_%s.pdf' % (train_no, train_step, simu_string))
	plt.close()

def plot_DLL(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt, train_step, simu_string, train_no, norm_string):

	if(norm_string == 'norm'):
		range_e = (-1,1)
		range_mu = (-1,1)
		range_k = (-1,1)
		range_p = (-1,1)
		range_d = (-1,1)
		range_bt = (-1,1)

	if(norm_string == 'raw'):
		range_e = (-40,20)
		range_mu = (-20,20)
		range_k = (-20,80)
		range_p = (-40,60)
		range_d = (-30,60)
		range_bt = (-30,60)

	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	bins_e = np.histogram(np.hstack((obs_simu_e, obs_emul_e)), bins=50)[1]
	ax[0,0].hist(obs_simu_e, bins_e, label='Simulated', alpha=0.5)
	ax[0,0].hist(obs_emul_e, bins_e, label='Emulated', alpha=0.5)
	ax[0,0].set_title('RichDLLe')
	ax[0,0].legend(loc='best')
	bins_mu = np.histogram(np.hstack((obs_simu_mu, obs_emul_mu)), bins=50)[1]
	ax[0,1].hist(obs_simu_mu, bins_mu, label='Simulated', alpha=0.5)
	ax[0,1].hist(obs_emul_mu, bins_mu, label='Emulated', alpha=0.5)
	ax[0,1].set_title('RichDLLmu')
	ax[0,1].legend(loc='best')
	bins_k = np.histogram(np.hstack((obs_simu_k, obs_emul_k)), bins=50)[1]
	ax[1,0].hist(obs_simu_k, bins_k, label='Simulated', alpha=0.5)
	ax[1,0].hist(obs_emul_k, bins_k, label='Emulated', alpha=0.5)
	ax[1,0].set_title('RichDLLk')
	ax[1,0].legend(loc='best')
	bins_p = np.histogram(np.hstack((obs_simu_p, obs_emul_p)), bins=50)[1]
	ax[1,1].hist(obs_simu_p, bins_p, label='Simulated', alpha=0.5)
	ax[1,1].hist(obs_emul_p, bins_p, label='Emulated', alpha=0.5)
	ax[1,1].set_title('RichDLLp')
	ax[1,1].legend(loc='best')
	bins_d = np.histogram(np.hstack((obs_simu_d, obs_emul_d)), bins=50)[1]
	ax[2,0].hist(obs_simu_d,  bins_d, label='Simulated', alpha=0.5)
	ax[2,0].hist(obs_emul_d,  bins_d, label='Emulated', alpha=0.5)
	ax[2,0].set_title('RichDLLd')
	ax[2,0].legend(loc='best')
	bins_bt = np.histogram(np.hstack((obs_simu_bt, obs_emul_bt)), bins=50)[1]
	ax[2,1].hist(obs_simu_bt, bins_bt, label='Simulated', alpha=0.5)
	ax[2,1].hist(obs_emul_bt, bins_bt, label='Emulated', alpha=0.5)
	ax[2,1].set_title('RichDLLbt')
	ax[2,1].legend(loc='best')

	plt.tight_layout()
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/outputs/output_%s/iteration/train_step_%s/plot_DLL_%s_%s.pdf' % (train_no, train_step, simu_string, norm_string))
	plt.close()

def plot_deltaDLL(simu_diff_e, simu_diff_mu, simu_diff_k, simu_diff_p, simu_diff_d, simu_diff_bt, train_step, simu_string, train_no):

	fig, ax =  plt.subplots(3, 2)

	simu_diff_e.hist(bins=50, ax=ax[0,0], figsize=(25,25))
	ax[0,0].set_title(simu_diff_e.name)
	simu_diff_mu.hist(bins=50, ax=ax[0,1], figsize=(25, 25))
	ax[0,1].set_title(simu_diff_mu.name)
	simu_diff_k.hist(bins=50, ax=ax[1,0], figsize=(25, 25))
	ax[1,0].set_title(simu_diff_k.name)
	simu_diff_p.hist(bins=50, ax=ax[1,1], figsize=(25, 25))
	ax[1,1].set_title(simu_diff_p.name)
	simu_diff_d.hist(bins=50, ax=ax[2,0], figsize=(25, 25))
	ax[2,0].set_title(simu_diff_d.name)
	simu_diff_bt.hist(bins=50, ax=ax[2,1], figsize=(25, 25))
	ax[2,1].set_title(simu_diff_bt.name)

	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/outputs/output_%s/iteration/train_step_%s/plot_deltaDLL_%s.pdf' % (train_no, train_step, simu_string))
	plt.close()
'''
def plot_diff_truevsemu(simu_diff, train_step, simustring, DLLstring):
	plt.hist(simu_diff, bins=50)
	plt.title('DLL%s: emulated - (%s)' % (DLLstring, simustring))
	plt.ylabel('Arbitrary Units')
	plt.xlabel('Difference')
	plt.savefig('results/plot_diff_trainstep_%s_%s_%s.pdf' % (train_step, simustring, DLLstring))
	plt.close()
'''
def plot_DLL_test(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt):

	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	range_e = (-40,20)
	range_mu = (-20,20)
	range_k = (-20,80)
	range_p = (-40,60)
	range_d = (-30,60)
	range_bt = (-30,60)

	bins_e = np.histogram(np.hstack((obs_simu_e, obs_emul_e)), bins=100)[1]
	ax[0,0].hist(obs_simu_e, bins_e, label='RichDLLe2', alpha=0.5)
	ax[0,0].hist(obs_emul_e, bins_e, label='RichDLLe3', alpha=0.5)
	ax[0,0].set_title('DLLe')
	ax[0,0].legend(loc='best')
	bins_mu = np.histogram(np.hstack((obs_simu_mu, obs_emul_mu)), bins=100)[1]
	ax[0,1].hist(obs_simu_mu, bins_mu, label='RichDLLmu2', alpha=0.5)
	ax[0,1].hist(obs_emul_mu, bins_mu, label='RichDLLmu3', alpha=0.5)
	ax[0,1].set_title('DLLmu')
	ax[0,1].legend(loc='best')
	bins_k = np.histogram(np.hstack((obs_simu_k, obs_emul_k)), bins=100)[1]
	ax[1,0].hist(obs_simu_k, bins_k, label='RichDLLk2', alpha=0.5)
	ax[1,0].hist(obs_emul_k, bins_k, label='RichDLLk3', alpha=0.5)
	ax[1,0].set_title('DLLk')
	ax[1,0].legend(loc='best')
	bins_p = np.histogram(np.hstack((obs_simu_p, obs_emul_p)), bins=100)[1]
	ax[1,1].hist(obs_simu_p, bins_p, label='RichDLLp2', alpha=0.5)
	ax[1,1].hist(obs_emul_p, bins_p, label='RichDLLp3', alpha=0.5)
	ax[1,1].set_title('DLLp')
	ax[1,1].legend(loc='best')
	bins_d = np.histogram(np.hstack((obs_simu_d, obs_emul_d)), bins=100)[1]
	ax[2,0].hist(obs_simu_d, bins_d, label='RichDLLd2', alpha=0.5)
	ax[2,0].hist(obs_emul_d, bins_d, label='RichDLLd3', alpha=0.5)
	ax[2,0].set_title('DLLd')
	ax[2,0].legend(loc='best')
	bins_bt = np.histogram(np.hstack((obs_simu_bt, obs_emul_bt)), bins=100)[1]
	ax[2,1].hist(obs_simu_bt, bins_bt, label='RichDLLbt2', alpha=0.5)
	ax[2,1].hist(obs_emul_bt, bins_bt, label='RichDLLbt3', alpha=0.5)
	ax[2,1].set_title('DLLbt')
	ax[2,1].legend(loc='best')

	plt.tight_layout()
	plt.savefig('DLL_comparison.pdf')
	plt.close()

def phs_plot_DLL(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt, simu_string, plots_string):

	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	range_e = (-40,20)
	range_mu = (-20,20)
	range_k = (-20,80)
	range_p = (-40,60)
	range_d = (-30,60)
	range_bt = (-30,60)

	ax[0,0].hist(obs_simu_e, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,0].hist(obs_emul_e, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,0].set_title('RichDLLe')
	ax[0,0].legend(loc='best')
	ax[0,1].hist(obs_simu_mu, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,1].hist(obs_emul_mu, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,1].set_title('RichDLLmu')
	ax[0,1].legend(loc='best')
	ax[1,0].hist(obs_simu_k, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,0].hist(obs_emul_k, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,0].set_title('RichDLLk')
	ax[1,0].legend(loc='best')
	ax[1,1].hist(obs_simu_p, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,1].hist(obs_emul_p, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,1].set_title('RichDLLp')
	ax[1,1].legend(loc='best')
	ax[2,0].hist(obs_simu_d,  bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,0].hist(obs_emul_d,  bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,0].set_title('RichDLLd')
	ax[2,0].legend(loc='best')
	ax[2,1].hist(obs_simu_bt, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,1].hist(obs_emul_bt, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,1].set_title('RichDLLbt')
	ax[2,1].legend(loc='best')

	plt.tight_layout()
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/ps_plots/plot_DLL_%s_%s.pdf' % (plots_string, simu_string))
	plt.close()

def phs_plot_deltaDLL(simu_diff_e, simu_diff_mu, simu_diff_k, simu_diff_p, simu_diff_d, simu_diff_bt, simu_string, plots_string):

	fig, ax =  plt.subplots(3, 2)

	simu_diff_e.hist(bins=50, ax=ax[0,0], figsize=(25,25))
	ax[0,0].set_title(simu_diff_e.name)
	simu_diff_mu.hist(bins=50, ax=ax[0,1], figsize=(25, 25))
	ax[0,1].set_title(simu_diff_mu.name)
	simu_diff_k.hist(bins=50, ax=ax[1,0], figsize=(25, 25))
	ax[1,0].set_title(simu_diff_k.name)
	simu_diff_p.hist(bins=50, ax=ax[1,1], figsize=(25, 25))
	ax[1,1].set_title(simu_diff_p.name)
	simu_diff_d.hist(bins=50, ax=ax[2,0], figsize=(25, 25))
	ax[2,0].set_title(simu_diff_d.name)
	simu_diff_bt.hist(bins=50, ax=ax[2,1], figsize=(25, 25))
	ax[2,1].set_title(simu_diff_bt.name)

	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion/ps_plots/plot_deltaDLL_%s_%s.pdf' % (plots_string, simu_string))
	plt.close()

#Calculate overlap between two distributions
def histogram_intersection(DLL_data_1, DLL_data_2, bin_no=200, x_range=None):
   
   hist1, bins = np.histogram(DLL_data_1, bins=bin_no, range=x_range, density=True)
   hist2, _ = np.histogram(DLL_data_2, bins=bin_no, range=x_range, density=True)
   bins = np.diff(bins)
   sm = 0
   for i in range(len(bins)):
       sm += min(bins[i]*hist1[i], bins[i]*hist2[i])
   return sm
