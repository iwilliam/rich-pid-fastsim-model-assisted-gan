"""
This is a first attempt for the pre-training stage of the model-assisted GAN. It takes a vector of physics parameters + noise and uses this to generate a vector of DLL observable parameters i.e. P = p(p, pt, nTracks, + noise) gets mapped to Q = (DLLe, DLLmu, DLLk, DLLp, DLLd, DLLbt) where Q = S(P) and  S is simulation. The aim is to train an emulator E such that Q = S(P) = E(P)
"""

import numpy as np
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Input, Dense, Activation, Flatten, Reshape
from tensorflow.python.keras.layers import Conv1D, UpSampling1D
from tensorflow.python.keras.layers import LeakyReLU, Dropout, BatchNormalization, Lambda
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras import initializers
from tensorflow.python.keras import metrics

__version__ = '1.1'
__author__ = 'Ifan Williams, Saul Alonso-Monsalve, Leigh Howard Whitehead'
__email__ = 'iwilliam@cern.ch, saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'

class Networks(object):
	def __init__(self, params=64, observables=6, print_summary=False):

		''' Constructor.

        Args:
            noise_size: size of input noise to the generator.
            params: number of physics parameters.
            print_summary: print summary of the models.
        '''

		self.params = params
		self.observables = observables
		self.print_summary = print_summary

		self.E = None # emulator
		self.S = None # siamese
		self.SM = None # siamese model
		self.AM1 = None # adversarial model 1
		#self.G = None # generator
		#self.D = None # discriminator
		#self.GE = None # generator + emulator
		#self.DM = None # discriminator model
		#self.AM2 = None # adversarial model 2

	'''
	Emulator: generate identical observable parameters to those of the simulator S when both E and S are fed with the same input parameters
	'''
	def emulator(self):

		''' Emulator: generate identical images to those of the simulator S when both E and S 
        are fed with the same input parameters.

        Args:

        Returns: emulator model.
        ''' 

		if self.E:
			return self.E

		# input params
		# the model takes as input an array of shape (*, self.params = 6)
		input_params_shape = (self.params,)
		input_params_layer = Input(shape=input_params_shape, name='input_params')

		# architecture
		self.E = Dense(1024)(input_params_layer)
		self.E = LeakyReLU(0.2)(self.E)
		self.E = Dense(self.observables*128, kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.E)
		self.E = LeakyReLU(0.2)(self.E)
		self.E = Reshape((self.observables, 128))(self.E)
		self.E = UpSampling1D(size=2)(self.E)
		self.E = Conv1D(64, kernel_size=7, padding='valid')(self.E)
		self.E = LeakyReLU(0.2)(self.E)
		self.E = UpSampling1D(size=2)(self.E)
		self.E = Conv1D(1, kernel_size=7, padding='valid', activation='tanh')(self.E)

		# model
		self.E = Model(inputs=input_params_layer, outputs=self.E, name='emulator')

		# print
		if self.print_summary:
			print("Emulator")
			self.E.summary()

		return self.E

	'''
	Siamese: determine the similarity between output values produced by the simulator and emulator
	'''
	def siamese(self):

		''' Siamese: determine the similarity between images produced by the simulator and the emulator.

        Args:

        Returns: siamese model.
        '''

		if self.S:
			return self.S

		# input DLL images
		input_shape = (self.observables, 1)
		input_layer_anchor = Input(shape=input_shape, name='input_layer_anchor')
		input_layer_candid = Input(shape=input_shape, name='input_layer_candidate')
		input_layer = Input(shape=input_shape, name='input_layer')

		# siamese
		cnn = Conv1D(64, kernel_size=8, strides=2, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(input_layer)
		cnn = BatchNormalization()(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		cnn = Conv1D(128, kernel_size=5, strides=2, padding='same')(cnn)
		cnn = BatchNormalization()(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		cnn = Flatten()(cnn)
		cnn = Dense(256, activation='sigmoid')(cnn)
		cnn = Model(inputs=input_layer, outputs=cnn, name='cnn')

		# print
		if self.print_summary:
			print("Siamese CNN:")
			cnn.summary()

		# left and right encodings		 
		encoded_l = cnn(input_layer_anchor)
		encoded_r = cnn(input_layer_candid)

		# merge two encoded inputs with the L1 or L2 distance between them
		L1_distance = lambda x: K.abs(x[0]-x[1])
		L2_distance = lambda x: (x[0]-x[1]+K.epsilon())**2/(x[0]+x[1]+K.epsilon())
		both = Lambda(L2_distance)([encoded_l, encoded_r])
		prediction = Dense(1, activation='sigmoid')(both)

		# model
		self.S = Model([input_layer_anchor, input_layer_candid], outputs=prediction, name='siamese')

		# print
		if self.print_summary:
			print("Siamese:")
			self.S.summary()

		return self.S

	'''
	Siamese model
	'''
	def siamese_model(self, lr=0.002):
		''' Siamese model.

        Args:
            lr: learning rate.

        Returns: siamese model (compiled).
        '''

		if self.SM:
			return self.SM

		# optimizer
		optimizer = Adam(lr=lr, beta_1=0.5, beta_2=0.9)

		# input DLL values
		input_shape = (self.observables, 1)
		input_layer_anchor = Input(shape=input_shape, name='input_layer_anchor')
		input_layer_candid = Input(shape=input_shape, name='input_layer_candidate')
		input_layer = [input_layer_anchor, input_layer_candid]

		# discriminator
		siamese_ref = self.siamese()
		siamese_ref.trainable = True
		self.SM = siamese_ref(input_layer)

		# model
		self.SM = Model(inputs=input_layer, outputs=self.SM, name='siamese_model')
		self.SM.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.binary_accuracy])

		if self.print_summary:
			print("Siamese model")
			self.SM.summary()

		return self.SM

	
	'''
	Adversarial 1 model (adversarial pre-training phase) - this is where the emulator and siamese network are trained to enable the emulator to generate DLL values for a set of given physics inputs
	'''
	def adversarial1_model(self, lr=0.0002):
		''' Adversarial 1 model.

        Args:
            lr: learning rate.

        Returns: adversarial 1 model (compiled).
        '''

		if self.AM1:
			return self.AM1

		optimizer = Adam(lr=lr, beta_1=0.5, beta_2=0.9)

		# input 1: simulated DLL values
		input_obs_shape = (self.observables, 1)
		input_obs_layer = Input(shape=input_obs_shape, name='input_obs')

		# input 2: params
		input_params_shape = (self.params, )
		input_params_layer = Input(shape=input_params_shape, name='input_params')

		# emulator
		emulator_ref = self.emulator()
		emulator_ref.trainable = True
		self.AM1 = emulator_ref(input_params_layer)

		# siamese
		siamese_ref = self.siamese()
		siamese_ref.trainable = False
		self.AM1 = siamese_ref([input_obs_layer, self.AM1])

		# model
		input_layer = [input_obs_layer, input_params_layer]
		self.AM1 = Model(inputs=input_layer, outputs=self.AM1, name='adversarial_1_model')
		self.AM1.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.binary_accuracy])

		# print
		if self.print_summary:
			print("Adversarial 1 model:")
			self.AM1.summary()

		return self.AM1

	"""
	'''
	Discriminator: distinguish between true data observable parameters and observable parameters produced by the simulator (or images produced by the emulator to speed up the training process
	'''
	def discriminator(self):
		if self.D:
			return self.D

		# input layer - 1D tensor of observables
		input_shape = (self.observables,)
		input_layer = Input(shape=input_shape, name='input_layer')

		self.D = Dense(self.observables)(input_layer)
		self.D = LeakyReLU(0.2)(self.D)
		self.D = Dense(128)(self.D)
		self.D = LeakyReLU(0.2)(self.D)
		self.D = Dense(128)(self.D)
		self.D = LeakyReLU(0.2)(self.D)

		# architecture
		self.D = Dense(1)(self.D)
		self.D = Activation('sigmoid')(self.D)

		# model
		self.D = Model(inputs=input_layer, outputs=self.D, name='discriminator')

		# print
		print("Discriminator:")
		self.D.summary()

		return self.D


	'''
	Generator: produce parameters such that the simulator can use them to create a vector of observables that cannot be distinguished from the true data observables

	'''
	def generator(self):
		if self.G:
			return self.G

		# params
		dropout = 0.1

		# input noise
		input_noise_shape = (self.noise_size,)
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# architecture
		self.G = Dense(128)(input_noise_layer)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = Dense(128)(self.G)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = Dense(self.params, activation='tanh')(self.G)

		# model
		self.G = Model(inputs=input_noise_layer, outputs=self.G, name='generator')

		# print
		print("Generator:")
		self.G.summary()

		return self.G

	'''
	Generator + emulator: output emulated observable parameters from input noise
	'''
	def generator_emulator(self):
		if self.GE:
			return self.GE

		# input noise
		input_noise_shape = (self.noise_size,)
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# generator
		generator_ref = self.generator()
		generator_ref.trainable = False
		self.GE = generator_ref(input_noise_layer)

		# emulator
		emulator_ref = self.emulator()
		emulator_ref.trainable = False
		self.GE = emulator_ref(self.GE)

		# model
		self.GE = Model(inputs=input_noise_layer, outputs=self.GE, name='generator_emulator')

		# print
		print("Generator + Emulator:")
		self.GE.summary()

		return self.GE


	'''
	Discriminator model
	'''
	def discriminator_model(self):
		if self.DM:
			return self.DM

		# optimizer
		optimizer = Adam(lr=0.2)

		# input image
		input_shape = (self.observables, )
		input_layer = Input(shape=input_shape, name='input_layer')

		# discriminator
		discriminator_ref = self.discriminator()
		discriminator_ref.trainable = True
		self.DM = discriminator_ref(input_layer)

		# model
		self.DM = Model(inputs=input_layer, outputs=self.DM, name='discriminator_model2')
		self.DM.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.categorical_accuracy])

		print("Discriminator model")
		self.DM.summary()

		return self.DM


	'''
	Adversarial 2 model (adversarial training phase) - this is where the discriminator is trained and the generator is trained to predict a set of parameters that best match the true data
	'''
	def adversarial2_model(self):
		if self.AM2:
			return self.AM2

		optimizer = Adam(lr=0.001, decay=0.0001)

		# input noise
		input_noise_shape = (self.noise_size,)
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# generator
		generator_ref = self.generator()
		generator_ref.trainable = True
		self.AM2 = generator_ref(input_noise_layer)

		# emulator
		emulator_ref = self.emulator()
		emulator_ref.trainable = False
		self.AM2 = emulator_ref(self.AM2)

		# discriminator
		discriminator_ref = self.discriminator()
		discriminator_ref.trainable = False
		self.AM2 = discriminator_ref(self.AM2)

		# model
		self.AM2 = Model(inputs=input_noise_layer, outputs=self.AM2, name='adversarial_2_model')
		self.AM2.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['acc'])

		# print
		print("Adversarial 2 model")
		self.AM2.summary()

		return self.AM2
		"""
