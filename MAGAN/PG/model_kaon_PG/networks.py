"""
This is a second attempt for the pre-training stage of the model-assisted GAN. It takes a vector of physics parameters + noise and uses this to generate a vector of DLL observable parameters i.e. P = p(p, pt, nTracks, + noise) gets mapped to Q = (DLLe, DLLmu, DLLk, DLLp, DLLd, DLLbt) where Q = S(P) and  S is simulation. The aim is to train an emulator E such that Q = S(P) = E(P)
"""

import numpy as np
import tensorflow
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Input, Dense, Activation, Flatten, Reshape, concatenate
from tensorflow.python.keras.layers import Conv1D, UpSampling1D, Conv2DTranspose
from tensorflow.python.keras.layers import LeakyReLU, Dropout, BatchNormalization, Lambda
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras import initializers
from tensorflow.python.keras import metrics
# Include if using tf v2
#from tensorflow_addons.layers import InstanceNormalization

__version__ = '1.1'
__author__ = 'Ifan Williams, Saul Alonso-Monsalve, Leigh Howard Whitehead'
__email__ = 'iwilliam@cern.ch, saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'


def Conv1DTranspose(input_tensor, filters, kernel_size, strides, padding, kernel_initializer):
    """
        input_tensor: tensor, with the shape (batch_size, time_steps, dims)
        filters: int, output dimension, i.e. the output tensor will have the shape of (batch_size, time_steps, filters)
        kernel_size: int, size of the convolution kernel
        strides: int, convolution step size
        padding: 'same' | 'valid'
        kernel_initializer: 'many choices'
    """
    x = Lambda(lambda x: K.expand_dims(x, axis=2))(input_tensor)
    x = Conv2DTranspose(filters=filters, kernel_size=(kernel_size, 1), strides=(strides, 1), padding=padding, kernel_initializer=kernel_initializer)(x)
    x = Lambda(lambda x: K.squeeze(x, axis=2))(x)
    return x

class Networks(object):
	def __init__(self, params_physics=5, params_noise=59, observables=6, print_summary=False):
		''' Constructor.

        Args:
        	params_physics: number of physics parameters.
        	params_noise: number of noise inputs.
            observables: number of observables.
            print_summary: print summary of the models.
        '''

		self.params_physics = params_physics
		self.params_noise = params_noise
		self.observables = observables
		self.print_summary = print_summary

		self.E = None # emulator
		self.S = None # siamese
		self.SM = None # siamese model
		self.AM1 = None # adversarial model 1
		#self.G = None # generator
		#self.D = None # discriminator
		#self.GE = None # generator + emulator
		#self.DM = None # discriminator model
		#self.AM2 = None # adversarial model 2

	def emulator(self):
		''' Emulator: generate identical images to those of the simulator S when both E and S 
        are fed with the same input parameters.

        Args:

        Returns: emulator model.
        ''' 

		if self.E:
			return self.E

		# input params
		# the model takes as input an array of shape (*, self.params = 6)
		input_params_shape = (self.params_physics, )
		input_noise_shape = (self.params_noise, )
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')
		input_params_layer = Input(shape=input_params_shape, name='input_params')
		input_layer = concatenate([input_noise_layer, input_params_layer], axis=-1)

		# architecture
		# old (with improved features)
		self.E = Reshape((self.observables//3, 32))(input_layer)
		self.E = UpSampling1D(size=3)(self.E)
		self.E = Conv1D(16, kernel_size=7, padding='same')(self.E)
		self.E = LeakyReLU(0.2)(self.E)
		self.E = UpSampling1D(size=2)(self.E)
		self.E = Conv1D(8, kernel_size=7, padding='valid')(self.E)
		self.E = LeakyReLU(0.2)(self.E)
		self.E = UpSampling1D(size=2)(self.E)
		self.E = Conv1D(4, kernel_size=7, padding='valid')(self.E)
		self.E = LeakyReLU(0.2)(self.E)
		self.E = UpSampling1D(size=2)(self.E)
		self.E = Conv1D(1, kernel_size=7, padding='valid')(self.E)
		self.E = Flatten()(self.E)
		self.E = Activation('tanh')(self.E)
		# new
		"""
		self.E = Reshape((self.observables//3, self.params//2))(input_params_layer)
		self.E = Conv1DTranspose(self.E, 32, kernel_size=2, strides=1, padding='valid',
			kernel_initializer=initializers.RandomNormal(stddev=0.02))
		self.E = InstanceNormalization(axis=-1, center=True, scale=True)(self.E)
		self.E = Activation('relu')(self.E)
		#self.E = Dropout(0.0)(self.E)
		self.E = Conv1DTranspose(self.E, 16, kernel_size=2, strides=1, padding='valid',
			kernel_initializer=initializers.RandomNormal(stddev=0.02))
		self.E = InstanceNormalization(axis=-1, center=True, scale=True)(self.E)
		self.E = Activation('relu')(self.E)
		#self.E = Dropout(0.0)(self.E)
		self.E = Conv1DTranspose(self.E, 8, kernel_size=2, strides=1, padding='valid',
			kernel_initializer=initializers.RandomNormal(stddev=0.02))
		self.E = InstanceNormalization(axis=-1, center=True, scale=True)(self.E)
		self.E = Activation('relu')(self.E)
		#self.E = Dropout(0.0)(self.E)
		self.E = Conv1DTranspose(self.E, 1, kernel_size=2, strides=1, padding='valid',
			kernel_initializer=initializers.RandomNormal(stddev=0.02))
		self.E = Activation('tanh')(self.E)
		"""
		# model
		self.E = Model(inputs=[input_noise_layer, input_params_layer], outputs=self.E, name='emulator')

		# print
		if self.print_summary:
			print("Emulator")
			self.E.summary()

		return self.E



	def siamese(self):
		''' Siamese: determine the similarity between images produced by the simulator and the emulator.

        Args:

        Returns: siamese model.
        '''

		if self.S:
			return self.S

		# input DLL images
		input_shape_obs = (self.observables, )
		input_shape_params = (self.params_physics, )
		input_shape = (self.observables + self.params_physics, )

		input_layer_anchor = Input(shape=input_shape_obs, name='input_layer_anchor')
		input_layer_candid = Input(shape=input_shape_obs, name='input_layer_candidate')
		input_layer_params = Input(shape=input_shape_params, name='input_layer_params')
		input_layer_anchor_p = concatenate([input_layer_anchor, input_layer_params], axis=-1)
		input_layer_candid_p = concatenate([input_layer_candid, input_layer_params], axis=-1)
		input_layer = Input(shape=input_shape, name='input_layer')


		# siamese
		# old (with improved features)
		cnn = Reshape((self.observables+self.params_physics, 1))(input_layer)
		cnn = Conv1D(64, kernel_size=8, strides=3, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
		cnn = BatchNormalization()(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		cnn = Conv1D(128, kernel_size=5, strides=2, padding='same')(cnn)
		cnn = BatchNormalization()(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		cnn = Flatten()(cnn)
		cnn = Activation('sigmoid')(cnn)
		# new
		"""
		cnn = Conv1D(16, kernel_size=8, strides=2, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(input_layer)
		cnn = LeakyReLU(0.2)(cnn)
		#cnn = Dropout(dropout)(cnn)
		cnn = Conv1D(32, kernel_size=5, strides=2, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
		cnn = InstanceNormalization(axis=-1, center=True, scale=True)(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		#cnn = Dropout(dropout)(cnn)
		cnn = Conv1D(64, kernel_size=5, strides=2, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
		cnn = InstanceNormalization(axis=-1, center=True, scale=True)(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		#cnn = Dropout(dropout)(cnn)
		cnn = Conv1D(128, kernel_size=5, strides=2, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(cnn)
		cnn = InstanceNormalization(axis=-1, center=True, scale=True)(cnn)
		cnn = LeakyReLU(0.2)(cnn)
		#cnn = Dropout(dropout)(cnn)
		cnn = Flatten()(cnn)
		cnn = Activation('sigmoid')(cnn)
		"""
		# model
		cnn = Model(inputs=input_layer, outputs=cnn, name='cnn')


		# print
		if self.print_summary:
			print("Siamese CNN:")
			cnn.summary()

		# left and right encodings		 
		encoded_l = cnn(input_layer_anchor_p)
		encoded_r = cnn(input_layer_candid_p)

		# merge two encoded inputs with the L1 or L2 distance between them
		L1_distance = lambda x: K.abs(x[0]-x[1])
		L2_distance = lambda x: (x[0]-x[1]+K.epsilon())**2/(x[0]+x[1]+K.epsilon())
		both = Lambda(L2_distance)([encoded_l, encoded_r])
		prediction = Dense(1, activation='sigmoid')(both)

		# model
		self.S = Model([input_layer_anchor, input_layer_candid, input_layer_params], outputs=prediction, name='siamese')

		# print
		if self.print_summary:
			print("Siamese:")
			self.S.summary()

		return self.S

	def siamese_model(self, lr=0.002):
		''' Siamese model.

        Args:
            lr: learning rate.

        Returns: siamese model (compiled).
        '''

		if self.SM:
			return self.SM

		# optimizer
		optimizer = tensorflow.keras.optimizers.Adam(lr=lr, beta_1=0.5, beta_2=0.9)

		# input DLL values
		input_shape_obs = (self.observables, )
		input_shape_params = (self.params_physics, )
		input_layer_anchor = Input(shape=input_shape_obs, name='input_layer_anchor')
		input_layer_candid = Input(shape=input_shape_obs, name='input_layer_candidate')
		input_layer_params = Input(shape=input_shape_params, name='input_layer_params')
		input_layer = [input_layer_anchor, input_layer_candid, input_layer_params]

		# discriminator
		siamese_ref = self.siamese()
		siamese_ref.trainable = True
		self.SM = siamese_ref(input_layer)

		# model
		self.SM = Model(inputs=[input_layer_anchor, input_layer_candid, input_layer_params], outputs=self.SM, name='siamese_model')
		self.SM.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.binary_accuracy])

		if self.print_summary:
			print("Siamese model")
			self.SM.summary()

		return self.SM

	
	def adversarial1_model(self, lr=0.0002):
		''' Adversarial 1 model.

        Args:
            lr: learning rate.

        Returns: adversarial 1 model (compiled).
        '''

		if self.AM1:
			return self.AM1

		optimizer = tensorflow.keras.optimizers.Adam(lr=lr, beta_1=0.5, beta_2=0.9)

		# input 1: simulated DLL values
		input_obs_shape = (self.observables, )
		input_obs_layer = Input(shape=input_obs_shape, name='input_obs_layer')

		# input 2: noise params
		input_noise_shape = (self.params_noise, )
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise_layer')

		# input 3: physics params
		input_params_shape = (self.params_physics, )
		input_params_layer = Input(shape=input_params_shape, name='input_params_layer')

		# emulator
		emulator_ref = self.emulator()
		emulator_ref.trainable = True
		self.AM1 = emulator_ref([input_noise_layer, input_params_layer])

		# siamese
		siamese_ref = self.siamese()
		siamese_ref.trainable = False
		self.AM1 = siamese_ref([input_obs_layer, self.AM1, input_params_layer])

		# model
		input_layer = [input_obs_layer, input_noise_layer, input_params_layer]
		self.AM1 = Model(inputs=input_layer, outputs=self.AM1, name='adversarial_1_model')
		self.AM1.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.binary_accuracy])

		# print
		if self.print_summary:
			print("Adversarial 1 model:")
			self.AM1.summary()

		return self.AM1

	"""
	'''
	Discriminator: distinguish between true data observable parameters and observable parameters produced by the simulator (or images produced by the emulator to speed up the training process
	'''
	def discriminator(self):
		if self.D:
			return self.D

		# input layer - 1D tensor of observables
		input_shape = (self.observables,)
		input_layer = Input(shape=input_shape, name='input_layer')

		self.D = Dense(self.observables)(input_layer)
		self.D = LeakyReLU(0.2)(self.D)
		self.D = Dense(128)(self.D)
		self.D = LeakyReLU(0.2)(self.D)
		self.D = Dense(128)(self.D)
		self.D = LeakyReLU(0.2)(self.D)

		# architecture
		self.D = Dense(1)(self.D)
		self.D = Activation('sigmoid')(self.D)

		# model
		self.D = Model(inputs=input_layer, outputs=self.D, name='discriminator')

		# print
		print("Discriminator:")
		self.D.summary()

		return self.D


	'''
	Generator: produce parameters such that the simulator can use them to create a vector of observables that cannot be distinguished from the true data observables

	'''
	def generator(self):
		if self.G:
			return self.G

		# params
		dropout = 0.1

		# input noise
		input_noise_shape = (self.noise_size,)
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# architecture
		self.G = Dense(128)(input_noise_layer)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = Dense(128)(self.G)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = Dense(self.params, activation='tanh')(self.G)

		# model
		self.G = Model(inputs=input_noise_layer, outputs=self.G, name='generator')

		# print
		print("Generator:")
		self.G.summary()

		return self.G

	'''
	Generator + emulator: output emulated observable parameters from input noise
	'''
	def generator_emulator(self):
		if self.GE:
			return self.GE

		# input noise
		input_noise_shape = (self.noise_size,)
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# generator
		generator_ref = self.generator()
		generator_ref.trainable = False
		self.GE = generator_ref(input_noise_layer)

		# emulator
		emulator_ref = self.emulator()
		emulator_ref.trainable = False
		self.GE = emulator_ref(self.GE)

		# model
		self.GE = Model(inputs=input_noise_layer, outputs=self.GE, name='generator_emulator')

		# print
		print("Generator + Emulator:")
		self.GE.summary()

		return self.GE


	'''
	Discriminator model
	'''
	def discriminator_model(self):
		if self.DM:
			return self.DM

		# optimizer
		optimizer = Adam(lr=0.2)

		# input image
		input_shape = (self.observables, )
		input_layer = Input(shape=input_shape, name='input_layer')

		# discriminator
		discriminator_ref = self.discriminator()
		discriminator_ref.trainable = True
		self.DM = discriminator_ref(input_layer)

		# model
		self.DM = Model(inputs=input_layer, outputs=self.DM, name='discriminator_model2')
		self.DM.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.categorical_accuracy])

		print("Discriminator model")
		self.DM.summary()

		return self.DM


	'''
	Adversarial 2 model (adversarial training phase) - this is where the discriminator is trained and the generator is trained to predict a set of parameters that best match the true data
	'''
	def adversarial2_model(self):
		if self.AM2:
			return self.AM2

		optimizer = Adam(lr=0.001, decay=0.0001)

		# input noise
		input_noise_shape = (self.noise_size,)
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# generator
		generator_ref = self.generator()
		generator_ref.trainable = True
		self.AM2 = generator_ref(input_noise_layer)

		# emulator
		emulator_ref = self.emulator()
		emulator_ref.trainable = False
		self.AM2 = emulator_ref(self.AM2)

		# discriminator
		discriminator_ref = self.discriminator()
		discriminator_ref.trainable = False
		self.AM2 = discriminator_ref(self.AM2)

		# model
		self.AM2 = Model(inputs=input_noise_layer, outputs=self.AM2, name='adversarial_2_model')
		self.AM2.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['acc'])

		# print
		print("Adversarial 2 model")
		self.AM2.summary()

		return self.AM2
		"""
