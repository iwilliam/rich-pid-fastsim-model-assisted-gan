import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as sp
#import seaborn as sns
#sns.set(style="darkgrid")

bins_array = [2000, 4000, 6000, 8000, 10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000, 26000, 28000, 30000,
32000, 34000, 36000, 38000, 40000, 42000, 44000, 46000, 48000, 50000, 52000, 54000, 56000, 58000, 
60000,  62000,  64000,  66000,  68000, 70000, 72000, 74000, 76000, 78000, 80000, 82000, 84000, 86000, 88000,
90000,  92000,  94000,  96000,  98000, 100000]
ticks_array = [2, 16, 30, 44, 58, 72, 86, 100]

def plot_inputs(*args, norm_string, train_no):

	l = []

	for arg in args:
		l.append(arg)

	quotient, remainder = divmod(len(l), 3)
	
	if(remainder != 0):
		quotient += 1

	fig, ax =  plt.subplots(quotient, 3)
	
	arg_no = 0
	for i in range(quotient):
		for j in range(3):
			try:
				l[arg_no].hist(bins=50, ax=ax[i,j], figsize=(25, 25))
				ax[i,j].set_title(l[arg_no].name)
			except IndexError:
				print("Skipping...")
			arg_no += 1			
	
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/plot_inputs_%s.pdf' % (train_no, norm_string) )
	plt.close()

def plot_outputs(*args, norm_string, train_no):

	l = []

	for arg in args:
		l.append(arg)

	quotient, remainder = divmod(len(l), 3)
	
	if(remainder != 0):
		quotient += 1

	fig, ax =  plt.subplots(quotient, 3)
	
	arg_no = 0
	for i in range(quotient):
		for j in range(3):
			try:
				l[arg_no].hist(bins=50, ax=ax[i,j], figsize=(25, 25))
				ax[i,j].set_title(l[arg_no].name)
			except IndexError:
				print("Skipping...")
			arg_no += 1			
	
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/plot_outputs_%s.pdf' % (train_no, norm_string) )
	plt.close()


def plot_history_full(d1_hist, d2_hist, d_hist, g_hist, a1_hist, a2_hist):
	# plot loss
	plt.subplot(2, 1, 1)
	plt.plot(d1_hist, label='S-real')
	plt.plot(d2_hist, label='S-fake')
	plt.plot(d_hist, label='S')
	plt.plot(g_hist, label='A')
	plt.title('Model losses (Top)/Discriminator Accuracy (Bottom)')
	plt.ylabel('Loss')
	plt.xlabel('Batch')
	plt.legend()
	# plot discriminator accuracy
	plt.subplot(2, 1, 2)
	plt.ylabel('Acc')
	plt.xlabel('Batch')
	plt.plot(a1_hist, label='Acc-real')
	plt.plot(a2_hist, label='Acc-fake')
	plt.legend()

	# save plot to file
	#plt.savefig('results/plot_loss_acc.pdf')
	plt.close()

def plot_history(d_hist, g_hist, train_no):
	# plot loss
	plt.subplot(2, 1, 1)
	plt.plot(d_hist, label='S', linestyle='dashed', linewidth='1.0')
	plt.plot(g_hist, label='A', linestyle='dashed', linewidth='1.0')
	plt.title('Model losses (Top)')
	plt.ylabel('Loss')
	plt.xlabel('Epoch')
	plt.legend()

	# save plot to file
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/plot_loss.pdf' % train_no)
	plt.close()

def plot_correlation(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt, train_step, epoch_no, simu_string, train_no):
	
	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	h1 = ax[0,0].hist2d(obs_emul_e, obs_simu_e, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[0,0].set_title('RichDLLe')
	ax[0,0].set_ylabel('Simulated')
	ax[0,0].set_xlabel('Emulated')
	ax[0,0].set_ylim(-0.75, 0.75)
	ax[0,0].set_xlim(-0.75, 0.75)
	plt.colorbar(h1[3], ax=ax[0,0])
	h2 = ax[0,1].hist2d(obs_emul_mu, obs_simu_mu, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[0,1].set_title('RichDLLmu')
	ax[0,1].set_ylabel('Simulated')
	ax[0,1].set_xlabel('Emulated')
	ax[0,1].set_ylim(-0.75, 0.75)
	ax[0,1].set_xlim(-0.75, 0.75)
	plt.colorbar(h2[3], ax=ax[0,1])
	h3 = ax[1,0].hist2d(obs_emul_k, obs_simu_k, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[1,0].set_title('RichDLLk')
	ax[1,0].set_ylabel('Simulated')
	ax[1,0].set_xlabel('Emulated')
	ax[1,0].set_ylim(-0.75, 0.75)
	ax[1,0].set_xlim(-0.75, 0.75)
	plt.colorbar(h3[3], ax=ax[1,0])
	h4 = ax[1,1].hist2d(obs_emul_p, obs_simu_p, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[1,1].set_title('RichDLLp')
	ax[1,1].set_ylabel('Simulated')
	ax[1,1].set_xlabel('Emulated')
	ax[1,1].set_ylim(-0.75, 0.75)
	ax[1,1].set_xlim(-0.75, 0.75)
	plt.colorbar(h4[3], ax=ax[1,1])
	h4 = ax[2,0].hist2d(obs_emul_d, obs_simu_d,  bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[2,0].set_title('RichDLLd')
	ax[2,0].set_ylabel('Simulated')
	ax[2,0].set_xlabel('Emulated')
	ax[2,0].set_ylim(-0.75, 0.75)
	ax[2,0].set_xlim(-0.75, 0.75)
	plt.colorbar(h4[3], ax=ax[2,0])
	h5 = ax[2,1].hist2d(obs_emul_bt, obs_simu_bt, bins=25, range=((-0.75, 0.75), (-0.75, 0.75)))
	ax[2,1].set_title('RichDLLbt')
	ax[2,1].set_ylabel('Simulated')
	ax[2,1].set_xlabel('Emulated')
	ax[2,1].set_ylim(-0.75, 0.75)
	ax[2,1].set_xlim(-0.75, 0.75)
	plt.colorbar(h5[3], ax=ax[2,1])

	plt.tight_layout()
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/epoch_%s/iteration/train_step_%s/plot_correlation_%s.pdf' % (train_no, epoch_no, train_step, simu_string))
	plt.close()

def plot_DLL(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt, train_step, epoch_no, simu_string, train_no, norm_string):

	# plot DLL
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	bins_e = np.histogram(np.hstack((obs_simu_e, obs_emul_e)), bins=50)[1]
	ax[0,0].hist(obs_simu_e, bins_e, label='Simulated', alpha=0.5)
	ax[0,0].hist(obs_emul_e, bins_e, label='Emulated', alpha=0.5)
	ax[0,0].set_title('RichDLLe')
	ax[0,0].legend(loc='best')
	bins_mu = np.histogram(np.hstack((obs_simu_mu, obs_emul_mu)), bins=50)[1]
	ax[0,1].hist(obs_simu_mu, bins_mu, label='Simulated', alpha=0.5)
	ax[0,1].hist(obs_emul_mu, bins_mu, label='Emulated', alpha=0.5)
	ax[0,1].set_title('RichDLLmu')
	ax[0,1].legend(loc='best')
	bins_k = np.histogram(np.hstack((obs_simu_k, obs_emul_k)), bins=50)[1]
	ax[1,0].hist(obs_simu_k, bins_k, label='Simulated', alpha=0.5)
	ax[1,0].hist(obs_emul_k, bins_k, label='Emulated', alpha=0.5)
	ax[1,0].set_title('RichDLLk')
	ax[1,0].legend(loc='best')
	bins_p = np.histogram(np.hstack((obs_simu_p, obs_emul_p)), bins=50)[1]
	ax[1,1].hist(obs_simu_p, bins_p, label='Simulated', alpha=0.5)
	ax[1,1].hist(obs_emul_p, bins_p, label='Emulated', alpha=0.5)
	ax[1,1].set_title('RichDLLp')
	ax[1,1].legend(loc='best')
	bins_d = np.histogram(np.hstack((obs_simu_d, obs_emul_d)), bins=50)[1]
	ax[2,0].hist(obs_simu_d,  bins_d, label='Simulated', alpha=0.5)
	ax[2,0].hist(obs_emul_d,  bins_d, label='Emulated', alpha=0.5)
	ax[2,0].set_title('RichDLLd')
	ax[2,0].legend(loc='best')
	bins_bt = np.histogram(np.hstack((obs_simu_bt, obs_emul_bt)), bins=50)[1]
	ax[2,1].hist(obs_simu_bt, bins_bt, label='Simulated', alpha=0.5)
	ax[2,1].hist(obs_emul_bt, bins_bt, label='Emulated', alpha=0.5)
	ax[2,1].set_title('RichDLLbt')
	ax[2,1].legend(loc='best')

	plt.tight_layout()
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/epoch_%s/iteration/train_step_%s/plot_DLL_%s_%s.pdf' % (train_no, epoch_no, train_step, simu_string, norm_string))
	plt.close()

def plot_deltaDLL(simu_diff_e, simu_diff_mu, simu_diff_k, simu_diff_p, simu_diff_d, simu_diff_bt, train_step, epoch_no, simu_string, train_no):

	fig, ax =  plt.subplots(3, 2)

	simu_diff_e.hist(bins=50, ax=ax[0,0], figsize=(25,25))
	ax[0,0].set_title(simu_diff_e.name)
	simu_diff_mu.hist(bins=50, ax=ax[0,1], figsize=(25, 25))
	ax[0,1].set_title(simu_diff_mu.name)
	simu_diff_k.hist(bins=50, ax=ax[1,0], figsize=(25, 25))
	ax[1,0].set_title(simu_diff_k.name)
	simu_diff_p.hist(bins=50, ax=ax[1,1], figsize=(25, 25))
	ax[1,1].set_title(simu_diff_p.name)
	simu_diff_d.hist(bins=50, ax=ax[2,0], figsize=(25, 25))
	ax[2,0].set_title(simu_diff_d.name)
	simu_diff_bt.hist(bins=50, ax=ax[2,1], figsize=(25, 25))
	ax[2,1].set_title(simu_diff_bt.name)

	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/epoch_%s/iteration/train_step_%s/plot_deltaDLL_%s.pdf' % (train_no, epoch_no, train_step, simu_string))
	plt.close()


def plot_momentum_dep(df_simucut1, df_gencut1, df_simucut2, df_gencut2, df_denom, train_no, epoch_no, train_step):

	df_denom = df_denom.copy()
	df_simucut1 = df_simucut1.copy()
	df_gencut1 = df_gencut1.copy()
	df_simucut2 = df_simucut2.copy()
	df_gencut2 = df_gencut2.copy()

	df_denom['P_bin'] = pd.cut(df_denom['TrackP'], bins=bins_array)
	df_simucut1['P_bin'] = pd.cut(df_simucut1['TrackP'], bins=bins_array)
	df_gencut1['P_bin'] = pd.cut(df_gencut1['TrackP'], bins=bins_array)
	df_simucut2['P_bin'] = pd.cut(df_simucut2['TrackP'], bins=bins_array)
	df_gencut2['P_bin'] = pd.cut(df_gencut2['TrackP'], bins=bins_array)

	df_denom = df_denom.groupby('P_bin').apply(lambda g: (g.shape[0]))
	df_simucut1 = df_simucut1.groupby('P_bin').apply(lambda g: (g.shape[0]))
	df_gencut1 = df_gencut1.groupby('P_bin').apply(lambda g: (g.shape[0]))
	df_simucut2 = df_simucut2.groupby('P_bin').apply(lambda g: (g.shape[0]))
	df_gencut2 = df_gencut2.groupby('P_bin').apply(lambda g: (g.shape[0]))

	df_simucut1 = pd.concat([df_denom.rename('Denom'), df_simucut1.rename('DLLk0_Numer')], axis=1)
	df_gencut1 = pd.concat([df_denom.rename('Denom'), df_gencut1.rename('GenDLLk0_Numer')], axis=1)
	df_simucut2 = pd.concat([df_denom.rename('Denom'), df_simucut2.rename('DLLk5_Numer')], axis=1)
	df_gencut2 = pd.concat([df_denom.rename('Denom'), df_gencut2.rename('GenDLLk5_Numer')], axis=1)

	df_simucut1['DLLk>0'] = df_simucut1.apply(lambda row: (row.DLLk0_Numer/row.Denom), axis = 1)
	df_gencut1['GenDLLk>0'] = df_gencut1.apply(lambda row: (row.GenDLLk0_Numer/row.Denom), axis = 1)
	df_simucut2['DLLk>5'] = df_simucut2.apply(lambda row: (row.DLLk5_Numer/row.Denom), axis = 1)
	df_gencut2['GenDLLk>5'] = df_gencut2.apply(lambda row: (row.GenDLLk5_Numer/row.Denom), axis = 1)

	df_simucut1 = df_simucut1['DLLk>0']
	df_gencut1 = df_gencut1['GenDLLk>0']
	df_simucut2 = df_simucut2['DLLk>5']
	df_gencut2 = df_gencut2['GenDLLk>5']

	print("---------------")
	print("K-S Statistics:")
	print("DLLk > 0 ", sp.ks_2samp(df_simucut1, df_gencut1))
	print("DLLk > 5 ", sp.ks_2samp(df_simucut2, df_gencut2))

	df = pd.concat([df_simucut1, df_gencut1, df_simucut2, df_gencut2], axis=1)
	ax = df.plot(style='.', color=['r','k','b','g'], alpha=0.3)
	tickrange = range(0, len(df)+1, 7)
	plt.xticks(tickrange, ticks_array)
	plt.ylabel('Pion MIS-ID efficiency')
	plt.xlabel('Momentum (GeV/c)')
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/outputs/output_%s/epoch_%s/iteration/train_step_%s/plot_momentum_dep.pdf' % (train_no, epoch_no, train_step))
	plt.close()


#Calculate overlap between two distributions
def histogram_intersection(DLL_data_1, DLL_data_2, bin_no=200, x_range=None):

	hist1, bins = np.histogram(DLL_data_1, bins=bin_no, range=x_range, density=True)
	hist2, _ = np.histogram(DLL_data_2, bins=bin_no, range=x_range, density=True)
	err_array = []
	bins = np.diff(bins)
	ol = 0
	for i in range(len(bins)):
		ol += min(bins[i]*hist1[i], bins[i]*hist2[i])
   # Determine uncertainty on overlap using bootstrapping
	for i in range(1000):
		hist_err, _ = np.histogram(np.random.choice(DLL_data_2, size=DLL_data_2.shape, replace=True), bins=bin_no, range=x_range, density=True)
		ol_err = 0
		for j in range(len(bins)):
			ol_err += min(bins[j]*hist1[j], bins[j]*hist_err[j])
		err_array.append(ol_err)
	err_array.sort()
	ol_err = err_array[842] - err_array[500]

	return ol, ol_err

"""
def plot_DLL_test(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt):

	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	range_e = (-40,20)
	range_mu = (-20,20)
	range_k = (-20,80)
	range_p = (-40,60)
	range_d = (-30,60)
	range_bt = (-30,60)

	bins_e = np.histogram(np.hstack((obs_simu_e, obs_emul_e)), bins=100)[1]
	ax[0,0].hist(obs_simu_e, bins_e, label='RichDLLe', alpha=0.5)
	ax[0,0].hist(obs_emul_e, bins_e, label='RichDLLe2', alpha=0.5)
	ax[0,0].set_title('DLLe')
	ax[0,0].legend(loc='best')
	bins_mu = np.histogram(np.hstack((obs_simu_mu, obs_emul_mu)), bins=100)[1]
	ax[0,1].hist(obs_simu_mu, bins_mu, label='RichDLLmu', alpha=0.5)
	ax[0,1].hist(obs_emul_mu, bins_mu, label='RichDLLmu2', alpha=0.5)
	ax[0,1].set_title('DLLmu')
	ax[0,1].legend(loc='best')
	bins_k = np.histogram(np.hstack((obs_simu_k, obs_emul_k)), bins=100)[1]
	ax[1,0].hist(obs_simu_k, bins_k, label='RichDLLk', alpha=0.5)
	ax[1,0].hist(obs_emul_k, bins_k, label='RichDLLk2', alpha=0.5)
	ax[1,0].set_title('DLLk')
	ax[1,0].legend(loc='best')
	bins_p = np.histogram(np.hstack((obs_simu_p, obs_emul_p)), bins=100)[1]
	ax[1,1].hist(obs_simu_p, bins_p, label='RichDLLp', alpha=0.5)
	ax[1,1].hist(obs_emul_p, bins_p, label='RichDLLp2', alpha=0.5)
	ax[1,1].set_title('DLLp')
	ax[1,1].legend(loc='best')
	bins_d = np.histogram(np.hstack((obs_simu_d, obs_emul_d)), bins=100)[1]
	ax[2,0].hist(obs_simu_d, bins_d, label='RichDLLd', alpha=0.5)
	ax[2,0].hist(obs_emul_d, bins_d, label='RichDLLd2', alpha=0.5)
	ax[2,0].set_title('DLLd')
	ax[2,0].legend(loc='best')
	bins_bt = np.histogram(np.hstack((obs_simu_bt, obs_emul_bt)), bins=100)[1]
	ax[2,1].hist(obs_simu_bt, bins_bt, label='RichDLLbt', alpha=0.5)
	ax[2,1].hist(obs_emul_bt, bins_bt, label='RichDLLbt2', alpha=0.5)
	ax[2,1].set_title('DLLbt')
	ax[2,1].legend(loc='best')

	plt.tight_layout()
	plt.savefig('DLL_comparison.pdf')
	plt.close()

def phs_plot_DLL(obs_simu_e, obs_emul_e, obs_simu_mu, obs_emul_mu, obs_simu_k, obs_emul_k, obs_simu_p, obs_emul_p, obs_simu_d, obs_emul_d, obs_simu_bt, obs_emul_bt, simu_string, plots_string):

	# plot correlation
	fig, ax =  plt.subplots(3, 2, figsize=(25, 25))

	range_e = (-40,20)
	range_mu = (-20,20)
	range_k = (-20,80)
	range_p = (-40,60)
	range_d = (-30,60)
	range_bt = (-30,60)

	ax[0,0].hist(obs_simu_e, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,0].hist(obs_emul_e, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,0].set_title('RichDLLe')
	ax[0,0].legend(loc='best')
	ax[0,1].hist(obs_simu_mu, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,1].hist(obs_emul_mu, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[0,1].set_title('RichDLLmu')
	ax[0,1].legend(loc='best')
	ax[1,0].hist(obs_simu_k, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,0].hist(obs_emul_k, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,0].set_title('RichDLLk')
	ax[1,0].legend(loc='best')
	ax[1,1].hist(obs_simu_p, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,1].hist(obs_emul_p, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[1,1].set_title('RichDLLp')
	ax[1,1].legend(loc='best')
	ax[2,0].hist(obs_simu_d,  bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,0].hist(obs_emul_d,  bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,0].set_title('RichDLLd')
	ax[2,0].legend(loc='best')
	ax[2,1].hist(obs_simu_bt, bins=100,  label='Simulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,1].hist(obs_emul_bt, bins=100,  label='Emulated', alpha=0.5, histtype='stepfilled', density=True)
	ax[2,1].set_title('RichDLLbt')
	ax[2,1].legend(loc='best')

	plt.tight_layout()
	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/ps_plots/plot_DLL_%s_%s.pdf' % (plots_string, simu_string))
	plt.close()

def phs_plot_deltaDLL(simu_diff_e, simu_diff_mu, simu_diff_k, simu_diff_p, simu_diff_d, simu_diff_bt, simu_string, plots_string):

	fig, ax =  plt.subplots(3, 2)

	simu_diff_e.hist(bins=50, ax=ax[0,0], figsize=(25,25))
	ax[0,0].set_title(simu_diff_e.name)
	simu_diff_mu.hist(bins=50, ax=ax[0,1], figsize=(25, 25))
	ax[0,1].set_title(simu_diff_mu.name)
	simu_diff_k.hist(bins=50, ax=ax[1,0], figsize=(25, 25))
	ax[1,0].set_title(simu_diff_k.name)
	simu_diff_p.hist(bins=50, ax=ax[1,1], figsize=(25, 25))
	ax[1,1].set_title(simu_diff_p.name)
	simu_diff_d.hist(bins=50, ax=ax[2,0], figsize=(25, 25))
	ax[2,0].set_title(simu_diff_d.name)
	simu_diff_bt.hist(bins=50, ax=ax[2,1], figsize=(25, 25))
	ax[2,1].set_title(simu_diff_bt.name)

	plt.savefig('/home/iw273/rich-pid-fastsim-model-assisted-gan/model_pion_PG/ps_plots/plot_deltaDLL_%s_%s.pdf' % (plots_string, simu_string))
	plt.close()

def plot_diff_truevsemu(simu_diff, train_step, simustring, DLLstring):
	plt.hist(simu_diff, bins=50)
	plt.title('DLL%s: emulated - (%s)' % (DLLstring, simustring))
	plt.ylabel('Arbitrary Units')
	plt.xlabel('Difference')
	plt.savefig('results/plot_diff_trainstep_%s_%s_%s.pdf' % (train_step, simustring, DLLstring))
	plt.close()

"""
