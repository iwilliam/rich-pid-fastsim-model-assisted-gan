# RICH-PID-fastsim-model-assisted-GAN

Model-Assisted GAN (MAGAN) implementation for fast simulation of the LHCb RICH PID response. Contains a module for the MAGAN and another for a trained conditional GAN (cGAN) that is used to benchmark the performance for the MAGAN.

Dependencies :-

 > pandas,
 > tensorflow2 (must be run with TensorFlow2!),
 > sklearn,
 > scipy,
 > keras

Can be run on a number of GPUs if specifying in the CUDA environ.

To activate the repo on gorfrog :-

 > conda activate TensorFlow2

To run the training :-

 > python train.py
