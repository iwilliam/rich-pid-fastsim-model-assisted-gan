"""
This is a second attempt for the training stage of a normal conditional GAN. It takes a vector of physics parameters + noise and uses this to generate a vector of DLL observable parameters i.e. P = p(p, pt, nTracks, + noise) gets mapped to Q = (DLLe, DLLmu, DLLk, DLLp, DLLd, DLLbt) where Q = S(P) and  S is simulation. The aim is to train an emulator E such that Q = S(P) = E(P)
"""

import numpy as np
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import Input, Dense, Activation, Flatten, Reshape, concatenate
from tensorflow.python.keras.layers import Conv1D, UpSampling1D
from tensorflow.python.keras.layers import LeakyReLU, Dropout, BatchNormalization, Lambda
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.optimizers import Adam
from tensorflow.python.keras import initializers
from tensorflow.python.keras import metrics

__version__ = '1.1'
__author__ = 'Ifan Williams, Saul Alonso-Monsalve, Leigh Howard Whitehead'
__email__ = 'iwilliam@cern.ch, saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'

class Networks(object):
	def __init__(self, params_physics=5, params_noise=59, observables=6, print_summary=False):

		''' Constructor.

        Args:
        	params_physics: number of physics parameters.
            params_noise: size of input noise to the generator.
            observables: number of observables.
            print_summary: print summary of the models.
        '''

		self.params_physics = params_physics
		self.params_noise = params_noise
		self.observables = observables
		self.print_summary = print_summary

		self.G = None # generator
		self.D = None # discriminator
		self.DM = None # discriminator model
		self.AM1 = None # adversarial model 1


	def generator(self):

		''' Generator: generate identical images to those of the simulator S when both E and S 
        are fed with the same input parameters.

        Args:

        Returns: generator model.
        ''' 

		if self.G:
			return self.G

		# input params
		# the model takes as input two arrays of shape (*, self.params_physics = 5) and (*, self.params_noise = 100)
		input_params_shape = (self.params_physics, )
		input_noise_shape = (self.params_noise, )
		input_params_layer = Input(shape=input_params_shape, name='input_params')
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')
		input_layer = concatenate([input_noise_layer, input_params_layer], axis=-1)

		# architecture
		self.G = Reshape((self.observables//3, 32))(input_layer)
		self.G = UpSampling1D(size=3)(self.G)
		self.G = Conv1D(16, kernel_size=7, padding='same')(self.G)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = UpSampling1D(size=2)(self.G)
		self.G = Conv1D(8, kernel_size=7, padding='valid')(self.G)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = UpSampling1D(size=2)(self.G)
		self.G = Conv1D(4, kernel_size=7, padding='valid')(self.G)
		self.G = LeakyReLU(0.2)(self.G)
		self.G = UpSampling1D(size=2)(self.G)
		self.G = Conv1D(1, kernel_size=7, padding='valid')(self.G)
		self.G = Flatten()(self.G)
		self.G = Activation('tanh')(self.G)

		# model
		self.G = Model(inputs=[input_noise_layer, input_params_layer], outputs=self.G, name='emulator')

		# print
		if self.print_summary:
			print("Generator")
			self.G.summary()

		return self.G

	
	def discriminator(self):

		'''
		Discriminator: distinguish between output values produced by the simulator and emulator

		Args:

        Returns: discriminator model.
        
		'''

		if self.D:
			return self.D

		# input DLL images
		input_shape_obs = (self.observables, )
		input_shape_params = (self.params_physics, )
		input_obs_layer = Input(shape=input_shape_obs, name='input_obs_layer')
		input_params_layer = Input(shape=input_shape_params, name='input_params_layer')
		input_layer = concatenate([input_obs_layer, input_params_layer], axis=-1)

		# siamese
		self.D = Reshape((self.observables+self.params_physics, 1))(input_layer)
		self.D = Conv1D(64, kernel_size=8, strides=3, padding='same', 
				     kernel_initializer=initializers.RandomNormal(stddev=0.02))(self.D)
		self.D = BatchNormalization()(self.D)
		self.D = LeakyReLU(0.2)(self.D)
		self.D = Conv1D(128, kernel_size=5, strides=2, padding='same')(self.D)
		self.D = BatchNormalization()(self.D)
		self.D = LeakyReLU(0.2)(self.D)
		self.D = Flatten()(self.D)
		self.D = Dense(1, activation='sigmoid')(self.D)

		# model
		self.D = Model(inputs=[input_obs_layer, input_params_layer], outputs=self.D, name='discriminator')

		# print
		if self.print_summary:
			print("Discriminator:")
			self.D.summary()

		return self.D


	def discriminator_model(self, lr=0.002):
		''' Discriminator model.

        Args:
            lr: learning rate.

        Returns: discriminator model (compiled).
        '''
		if self.DM:
			return self.DM

		# optimizer
		optimizer = Adam(lr=lr, beta_1=0.5, beta_2=0.9)

		# input DLL values
		input_shape_obs = (self.observables, )
		input_shape_params = (self.params_physics, )
		input_obs_layer = Input(shape=input_shape_obs, name='obs_layer')
		input_params_layer = Input(shape=input_shape_params, name='params_layer')
		input_layer = [input_obs_layer, input_params_layer]

		# discriminator
		discriminator_ref = self.discriminator()
		discriminator_ref.trainable = True
		self.DM = discriminator_ref(input_layer)

		# model
		self.DM = Model(inputs=[input_obs_layer, input_params_layer], outputs=self.DM, name='discriminator_model')
		self.DM.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.binary_accuracy])

		if self.print_summary:
			print("Discriminator model")
			self.DM.summary()

		return self.DM


	def adversarial1_model(self, lr=0.0002):
		''' Adversarial 1 model.

        Args:
            lr: learning rate.

        Returns: adversarial 1 model (compiled).
        '''
		if self.AM1:
			return self.AM1

		optimizer = Adam(lr=lr, beta_1=0.5, beta_2=0.9)

		# input 1: noise
		input_noise_shape = (self.params_noise, )
		input_noise_layer = Input(shape=input_noise_shape, name='input_noise')

		# input 2: physics params
		input_params_shape = (self.params_physics, )
		input_params_layer = Input(shape=input_params_shape, name='input_params')

		# emulator
		generator_ref = self.generator()
		generator_ref.trainable = True
		self.AM1 = generator_ref([input_noise_layer, input_params_layer])

		# discriminator
		discriminator_ref = self.discriminator()
		discriminator_ref.trainable = False
		self.AM1 = discriminator_ref([self.AM1, input_params_layer])

		# model
		input_layer = [input_noise_layer, input_params_layer]
		self.AM1 = Model(inputs=input_layer, outputs=self.AM1, name='adversarial_1_model')
		self.AM1.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=[metrics.binary_accuracy])

		# print
		if self.print_summary:
			print("Adversarial 1 model:")
			self.AM1.summary()

		return self.AM1
