"""
Training implementation
"""

__version__= '1.1'
__author__ = 'Ifan Williams, Saúl Alonso-Monsalve, Leigh Howard Whitehead'
__email__= 'ifan.williams@cern.ch, saul.alonso.monsalve@cern.ch, leigh.howard.whitehead@cern.ch'

import os
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# here one can specify the GPUs to use
os.environ['CUDA_DEVICE_ORDER']='PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES']= '1' 
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)
# Import relevant packages
import time
import math
import numpy as np
import random as rn
import pandas as pd
import matplotlib.pyplot as plt
from functions import plot_history, plot_deltaDLL, plot_inputs, plot_outputs, plot_correlation, plot_DLL, histogram_intersection, plot_momentum_dep
from networks import Networks
from tensorflow.python.keras import backend as K
from tensorflow.python.keras.models import Model
from tensorflow.python.keras import metrics
from tensorflow.python.keras.models import model_from_json
from sklearn.preprocessing import QuantileTransformer
from sklearn.utils import shuffle
from scipy.interpolate import interp1d
from sklearn.externals import joblib
import random
from random import seed
from random import choices
from argparse import ArgumentParser

# Ranges for the transformations
net_range = [-1,1]
gauss_range = [-5.5,5.5]

# Mapping functions
mapping = interp1d(gauss_range, net_range)
antimapping = interp1d(net_range, gauss_range)

# Remove for randomness - setting these (should) give reproducible results
np.random.seed(7)
rn.seed(15453)
tf.set_random_seed(1412)
#session_conf = tf.ConfigProto(intra_op_parallelism_threads=1,
#                              inter_op_parallelism_threads=1)
#sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
#K.set_session(sess)

plt.rcParams['agg.path.chunksize'] = 10000 #Needed for plotting lots of data

def get_args():
    ''' Retrieve arguments.

    Returns: arguments.
    '''
    parser = ArgumentParser(description='Conditional GAN Particle Gun')
    arg = parser.add_argument
    arg('--params_physics', type=int, default=5, help='Physics parameters')
    arg('--params_noise', type=int, default=59, help='Noise size (generator input)')
    arg('--observables', type=int, default=6, help='Number of observables')
    arg('--train', type=lambda x: (str(x).lower() == 'true'), default=True, help='Run training')
    arg('--train_steps', type=int, default=135001, help='Training steps')
    arg('--batch_size', type=int, default=32, help='Batch size')
    arg('--train_no', type=str, default=4, help='Training number')
    arg('--epochs', type=str, default=40, help='Number of epochs')
    arg('--print_summary', type=lambda x: (str(x).lower() == 'true'), default=False, help='Print summary of the models')
    arg('--data_path', type=str, default='/home/iw273/rich-pid-fastsim-model-assisted-gan/data/PID-train-data-PIONS-PG.hdf', help='Data file path')
    arg('--save_weights', type=lambda x: (str(x).lower() == 'true'), default=True, help='Save weights')
    arg('--output_path', type=str, default='/home/iw273/cGAN/model_pion_PG/outputs', help='Output path')
 
    args = parser.parse_args()
    return args

args = get_args()

class ConditionalGANPID(object):

	def __init__(self, params_physics=5, params_noise=59, observables=6, print_summary=False):
		''' Constructor.
        
        Args:
            params_physics: total number of physics parameters
            params_physics: total number of physics parameters
            observables: size of output DLL vector.
            print_summary: print summary of the models.
        '''

		self.params_physics = params_physics
		self.params_noise = params_noise
		self.observables = observables
		self.Networks = Networks(params_physics=params_physics, params_noise=params_noise, observables=observables, print_summary=print_summary)
		self.discriminator = self.Networks.discriminator_model()
		self.adversarial1 = self.Networks.adversarial1_model()
		self.generator = self.Networks.generator()

	# Function to save model weights
	def save_weights(self, path='output', train_no=1):
		''' Save model weights.

        Args:
            path: output path.
            train_no: training number
        '''
		print('Saving weights...')
		self.discriminator.save_weights(path + '/output_%s/weights/discriminator.h5' % train_no)
		self.generator.save_weights(path + '/output_%s/weights/generator.h5' % train_no)
		self.adversarial1.save_weights(path + '/output_%s/weights/adversarial1.h5' % train_no)


	def train(self, train_steps=90839, batch_size=32, train_no=1, epochs=10, data_path=None, save_weights=True, output_path=None):

		''' Training stage.

        Args:
            train_steps: number of pre-training steps (iterations).
            batch_size: batch size.
            training number: training number.
            train_no: training number.
            save_weights: save model weights.
            output_path: output path.
        '''

		print('Training for epochs = ', epochs)
		print('Training for train steps = ', train_steps)
		print('Training number = ', train_no)

		'''
		Training stage
		'''

		# Number of tracks for the training + validation sample
		#n_events = 3976698 + 100000
		#n_train = n_events - 100000

		# Number of tracks for the training + validation sample
		n_events = 4320062 + 100000
		n_train = n_events - 100000
 
		# Parameters for Gaussian noise
		mu = 0
		sigma = 1

		# import RICH simulation data

		print('Loading data...')

		pion_data = pd.read_hdf(data_path)

		#pion_data = pion_data[ (pion_data.TrackP >= 10000 ) & (pion_data.TrackP <= 100000 )]

		pion_data = pion_data.sample(n=n_events)
		pion_data = pion_data.reset_index(drop=True)

		pion_data_train = pion_data[:n_train]
		pion_data_test = pion_data[n_train:n_events]

		print("Producing training data...")

		# add all physics inputs

		# add all physics inputs
		P_pion_data_train = pion_data_train['TrackP']
		Pt_pion_data_train = pion_data_train['TrackPt']
		trackVertexX_pion_data_train = pion_data_train['TrackVertexX']
		trackVertexY_pion_data_train = pion_data_train['TrackVertexY']
		trackVertexZ_pion_data_train = pion_data_train['TrackVertexZ']
		#rich1EntryX_pion_data_train = pion_data_train['TrackRich1EntryX']
		#rich1EntryY_pion_data_train = pion_data_train['TrackRich1EntryY']
		#rich1ExitX_pion_data_train = pion_data_train['TrackRich1ExitX']
		#rich1ExitY_pion_data_train = pion_data_train['TrackRich1ExitY']
		#rich2EntryX_pion_data_train = pion_data_train['TrackRich2EntryX']
		#rich2EntryY_pion_data_train = pion_data_train['TrackRich2EntryY']
		#rich2ExitX_pion_data_train = pion_data_train['TrackRich2ExitX']
		#rich2ExitY_pion_data_train = pion_data_train['TrackRich2ExitY']

		# add different DLL outputs

		Dlle_pion_data_train = pion_data_train['RichDLLe']
		Dllmu_pion_data_train = pion_data_train['RichDLLmu']
		Dllk_pion_data_train = pion_data_train['RichDLLk']
		Dllp_pion_data_train = pion_data_train['RichDLLp']
		Dlld_pion_data_train = pion_data_train['RichDLLd']
		Dllbt_pion_data_train = pion_data_train['RichDLLbt']

		print("Producing plots of input and outputs...")

		# plot raw inputs/outputs

		#plot_inputs(P_pion_data_train, Pt_pion_data_train, trackVertexX_pion_data_train, trackVertexY_pion_data_train, trackVertexZ_pion_data_train, rich1EntryX_pion_data_train, rich1EntryY_pion_data_train,
		#	rich1ExitX_pion_data_train, rich1ExitY_pion_data_train, rich2EntryX_pion_data_train, rich2EntryY_pion_data_train, rich2ExitX_pion_data_train, rich2ExitY_pion_data_train,
		#	norm_string='raw', train_no=train_no)

		plot_inputs(P_pion_data_train, Pt_pion_data_train, trackVertexX_pion_data_train, trackVertexY_pion_data_train, trackVertexZ_pion_data_train,
			norm_string='raw', train_no=train_no)

		plot_outputs(Dlle_pion_data_train, Dllmu_pion_data_train, Dllk_pion_data_train, Dllp_pion_data_train, Dlld_pion_data_train, Dllbt_pion_data_train, 
			norm_string='raw', train_no=train_no)
		
		# convert to numpy array	
		P_pion_data_train = P_pion_data_train.to_numpy()
		Pt_pion_data_train = Pt_pion_data_train.to_numpy()
		trackVertexX_pion_data_train = trackVertexX_pion_data_train.to_numpy()
		trackVertexY_pion_data_train = trackVertexY_pion_data_train.to_numpy()
		trackVertexZ_pion_data_train = trackVertexZ_pion_data_train.to_numpy()
		#rich1EntryX_pion_data_train = rich1EntryX_pion_data_train.to_numpy()
		#rich1EntryY_pion_data_train = rich1EntryY_pion_data_train.to_numpy()
		#rich1ExitX_pion_data_train = rich1ExitX_pion_data_train.to_numpy()
		#rich1ExitY_pion_data_train = rich1ExitY_pion_data_train.to_numpy()
		#rich2EntryX_pion_data_train = rich2EntryX_pion_data_train.to_numpy()
		#rich2EntryY_pion_data_train = rich2EntryY_pion_data_train.to_numpy()
		#rich2ExitX_pion_data_train = rich2ExitX_pion_data_train.to_numpy()
		#rich2ExitY_pion_data_train = rich2ExitY_pion_data_train.to_numpy()

		Dlle_pion_data_train = Dlle_pion_data_train.to_numpy()
		Dllmu_pion_data_train = Dllmu_pion_data_train.to_numpy()
		Dllk_pion_data_train = Dllk_pion_data_train.to_numpy()
		Dllp_pion_data_train = Dllp_pion_data_train.to_numpy()
		Dlld_pion_data_train = Dlld_pion_data_train.to_numpy()
		Dllbt_pion_data_train = Dllbt_pion_data_train.to_numpy()

		# Reshape arrays
		P_pion_data_train = np.array(P_pion_data_train).reshape(-1, 1)
		Pt_pion_data_train = np.array(Pt_pion_data_train).reshape(-1, 1)
		trackVertexX_pion_data_train = np.array(trackVertexX_pion_data_train).reshape(-1, 1)
		trackVertexY_pion_data_train = np.array(trackVertexY_pion_data_train).reshape(-1, 1)
		trackVertexZ_pion_data_train = np.array(trackVertexZ_pion_data_train).reshape(-1, 1)
		#rich1EntryX_pion_data_train = np.array(rich1EntryX_pion_data_train).reshape(-1, 1)
		#rich1EntryY_pion_data_train = np.array(rich1EntryY_pion_data_train).reshape(-1, 1)
		#rich1ExitX_pion_data_train = np.array(rich1ExitX_pion_data_train).reshape(-1, 1)
		#rich1ExitY_pion_data_train = np.array(rich1ExitY_pion_data_train).reshape(-1, 1)
		#rich2EntryX_pion_data_train = np.array(rich2EntryX_pion_data_train).reshape(-1, 1)
		#rich2EntryY_pion_data_train = np.array(rich2EntryY_pion_data_train).reshape(-1, 1)
		#rich2ExitX_pion_data_train = np.array(rich2ExitX_pion_data_train).reshape(-1, 1)
		#rich2ExitY_pion_data_train = np.array(rich2ExitY_pion_data_train).reshape(-1, 1)

		Dlle_pion_data_train = np.array(Dlle_pion_data_train).reshape(-1, 1)
		Dllmu_pion_data_train = np.array(Dllmu_pion_data_train).reshape(-1, 1)
		Dllk_pion_data_train = np.array(Dllk_pion_data_train).reshape(-1, 1)
		Dllp_pion_data_train = np.array(Dllp_pion_data_train).reshape(-1, 1)
		Dlld_pion_data_train = np.array(Dlld_pion_data_train).reshape(-1, 1)
		Dllbt_pion_data_train = np.array(Dllbt_pion_data_train).reshape(-1, 1)

		#inputs_pion_data_train = np.concatenate((P_pion_data_train, Pt_pion_data_train,  trackVertexX_pion_data_train, trackVertexY_pion_data_train, trackVertexZ_pion_data_train, rich1EntryX_pion_data_train, 
		#rich1EntryY_pion_data_train, rich1ExitX_pion_data_train, rich1ExitY_pion_data_train, rich2EntryX_pion_data_train, rich2EntryY_pion_data_train, rich2ExitX_pion_data_train, rich2ExitY_pion_data_train), axis=1)

		inputs_pion_data_train = np.concatenate((P_pion_data_train, Pt_pion_data_train,  trackVertexX_pion_data_train, trackVertexY_pion_data_train, trackVertexZ_pion_data_train), axis=1)
		Dll_pion_data_train = np.concatenate((Dlle_pion_data_train, Dllmu_pion_data_train, Dllk_pion_data_train, Dllp_pion_data_train, Dlld_pion_data_train, Dllbt_pion_data_train), axis=1)

		# Transform inputs/outputs

		print('Transforming inputs and outputs using Quantile Transformer...')

		scaler_inputs = QuantileTransformer(output_distribution='normal', n_quantiles=int(1e5), subsample=int(1e10)).fit(inputs_pion_data_train)
		scaler_Dll = QuantileTransformer(output_distribution='normal', n_quantiles=int(1e5), subsample=int(1e10)).fit(Dll_pion_data_train)

		#joblib.dump(scaler_inputs, '/home/iw273/cGAN/model_pion/scaler_inputs.save')
		#joblib.dump(scaler_Dll, '/home/iw273/cGAN/model_pion/scaler_Dll.save')
		
		inputs_pion_data_train = scaler_inputs.transform(inputs_pion_data_train)
		Dll_pion_data_train = scaler_Dll.transform(Dll_pion_data_train)

		# Map to reduced range

		inputs_pion_data_train = mapping(inputs_pion_data_train)
		Dll_pion_data_train = mapping(Dll_pion_data_train)

		print("Producing plots of normalised inputs and outputs...")
		
		# plot normalised inputs/outputs

		plots_inputs_pion_data_train = np.hsplit(inputs_pion_data_train, self.params_physics)
		plots_Dll_pion_data_train = np.hsplit(Dll_pion_data_train, self.observables)

		P_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[0].flatten(), name='TrackP')
		Pt_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[1].flatten(), name='TrackPt')
		trackVertexX_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[2].flatten(), name='TrackVertexX')
		trackVertexY_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[3].flatten(), name='TrackVertexY')
		trackVertexZ_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[4].flatten(), name='TrackVertexY')
		#rich1EntryX_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[5].flatten(), name='TrackRich1EntryX')
		#rich1EntryY_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[6].flatten(), name='TrackRich1EntryY')
		#rich1ExitX_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[7].flatten(), name='TrackRich1ExitX')
		#rich1ExitY_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[8].flatten(), name='TrackRich1ExitY')
		#rich2EntryX_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[9].flatten(), name='TrackRich2EntryX')
		#rich2EntryY_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[10].flatten(), name='TrackRich2EntryY')
		#rich2ExitX_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[11].flatten(), name='TrackRich2ExitX')
		#rich2ExitY_plots_inputs_pion_data_train = pd.Series(data=plots_inputs_pion_data_train[12].flatten(), name='TrackRich2ExitY')

		Dlle_plots_Dll_pion_data_train = pd.Series(data=plots_Dll_pion_data_train[0].flatten(), name='RichDLLe')
		Dllmu_plots_Dll_pion_data_train = pd.Series(data=plots_Dll_pion_data_train[1].flatten(), name='RichDLLmu')
		Dllk_plots_Dll_pion_data_train = pd.Series(data=plots_Dll_pion_data_train[2].flatten(), name='RichDLLk')
		Dllp_plots_Dll_pion_data_train = pd.Series(data=plots_Dll_pion_data_train[3].flatten(), name='RichDLLp')
		Dlld_plots_Dll_pion_data_train = pd.Series(data=plots_Dll_pion_data_train[4].flatten(), name='RichDLLd')
		Dllbt_plots_Dll_pion_data_train = pd.Series(data=plots_Dll_pion_data_train[5].flatten(), name='RichDLLbt')

		#plot_inputs(P_plots_inputs_pion_data_train, Pt_plots_inputs_pion_data_train, trackVertexX_plots_inputs_pion_data_train, trackVertexY_plots_inputs_pion_data_train, trackVertexZ_plots_inputs_pion_data_train,
		#rich1EntryX_plots_inputs_pion_data_train, rich1EntryY_plots_inputs_pion_data_train, rich1ExitX_plots_inputs_pion_data_train, rich1ExitY_plots_inputs_pion_data_train, rich2EntryX_plots_inputs_pion_data_train,
		#rich2EntryY_plots_inputs_pion_data_train, rich2ExitX_plots_inputs_pion_data_train, rich2ExitY_plots_inputs_pion_data_train, norm_string='norm', train_no=train_no)

		plot_inputs(P_plots_inputs_pion_data_train, Pt_plots_inputs_pion_data_train, trackVertexX_plots_inputs_pion_data_train, trackVertexY_plots_inputs_pion_data_train, trackVertexZ_plots_inputs_pion_data_train,
		 norm_string='norm', train_no=train_no)

		plot_outputs(Dlle_plots_Dll_pion_data_train, Dllmu_plots_Dll_pion_data_train, Dllk_plots_Dll_pion_data_train, Dllp_plots_Dll_pion_data_train, Dlld_plots_Dll_pion_data_train, 
			Dllbt_plots_Dll_pion_data_train, norm_string='norm', train_no=train_no)
		
		# REPEATING FOR TESTING DATA

		print("Producing testing data...")

		# add all physics inputs

		# adding all physics inputs
		P_pion_data_test = pion_data_test['TrackP']
		Pt_pion_data_test = pion_data_test['TrackPt']
		trackVertexX_pion_data_test = pion_data_test['TrackVertexX']
		trackVertexY_pion_data_test = pion_data_test['TrackVertexY']
		trackVertexZ_pion_data_test = pion_data_test['TrackVertexZ']
		#rich1EntryX_pion_data_test = pion_data_test['TrackRich1EntryX']
		#rich1EntryY_pion_data_test = pion_data_test['TrackRich1EntryY']
		#rich1ExitX_pion_data_test = pion_data_test['TrackRich1ExitX']
		#rich1ExitY_pion_data_test = pion_data_test['TrackRich1ExitY']
		#rich2EntryX_pion_data_test = pion_data_test['TrackRich2EntryX']
		#rich2EntryY_pion_data_test = pion_data_test['TrackRich2EntryY']
		#rich2ExitX_pion_data_test = pion_data_test['TrackRich2ExitX']
		#rich2ExitY_pion_data_test = pion_data_test['TrackRich2ExitY']


		# add different DLL outputs
		Dlle_pion_data_test = pion_data_test['RichDLLe']
		Dllmu_pion_data_test = pion_data_test['RichDLLmu']
		Dllk_pion_data_test = pion_data_test['RichDLLk']
		Dllp_pion_data_test = pion_data_test['RichDLLp']
		Dlld_pion_data_test = pion_data_test['RichDLLd']
		Dllbt_pion_data_test = pion_data_test['RichDLLbt']

		# convert to numpy array
		P_pion_data_test = P_pion_data_test.to_numpy()
		Pt_pion_data_test = Pt_pion_data_test.to_numpy()
		trackVertexX_pion_data_test = trackVertexX_pion_data_test.to_numpy()
		trackVertexY_pion_data_test = trackVertexY_pion_data_test.to_numpy()
		trackVertexZ_pion_data_test = trackVertexZ_pion_data_test.to_numpy()
		#rich1EntryX_pion_data_test = rich1EntryX_pion_data_test.to_numpy()
		#rich1EntryY_pion_data_test = rich1EntryY_pion_data_test.to_numpy()
		#rich1ExitX_pion_data_test = rich1ExitX_pion_data_test.to_numpy()
		#rich1ExitY_pion_data_test = rich1ExitY_pion_data_test.to_numpy()
		#rich2EntryX_pion_data_test = rich2EntryX_pion_data_test.to_numpy()
		#rich2EntryY_pion_data_test = rich2EntryY_pion_data_test.to_numpy()
		#rich2ExitX_pion_data_test = rich2ExitX_pion_data_test.to_numpy()
		#rich2ExitY_pion_data_test = rich2ExitY_pion_data_test.to_numpy()

		Dlle_pion_data_test = Dlle_pion_data_test.to_numpy()
		Dllmu_pion_data_test = Dllmu_pion_data_test.to_numpy()
		Dllk_pion_data_test = Dllk_pion_data_test.to_numpy()
		Dllp_pion_data_test = Dllp_pion_data_test.to_numpy()
		Dlld_pion_data_test = Dlld_pion_data_test.to_numpy()
		Dllbt_pion_data_test = Dllbt_pion_data_test.to_numpy()

		# Reshape arrays

		P_pion_data_test = np.array(P_pion_data_test).reshape(-1, 1)
		Pt_pion_data_test = np.array(Pt_pion_data_test).reshape(-1, 1)
		trackVertexX_pion_data_test = np.array(trackVertexX_pion_data_test).reshape(-1, 1)
		trackVertexY_pion_data_test = np.array(trackVertexY_pion_data_test).reshape(-1, 1)
		trackVertexZ_pion_data_test = np.array(trackVertexZ_pion_data_test).reshape(-1, 1)
		#rich1EntryX_pion_data_test = np.array(rich1EntryX_pion_data_test).reshape(-1, 1)
		#rich1EntryY_pion_data_test = np.array(rich1EntryY_pion_data_test).reshape(-1, 1)
		#rich1ExitX_pion_data_test = np.array(rich1ExitX_pion_data_test).reshape(-1, 1)
		#rich1ExitY_pion_data_test = np.array(rich1ExitY_pion_data_test).reshape(-1, 1)
		#rich2EntryX_pion_data_test = np.array(rich2EntryX_pion_data_test).reshape(-1, 1)
		#rich2EntryY_pion_data_test = np.array(rich2EntryY_pion_data_test).reshape(-1, 1)
		#rich2ExitX_pion_data_test = np.array(rich2ExitX_pion_data_test).reshape(-1, 1)
		#rich2ExitY_pion_data_test = np.array(rich2ExitY_pion_data_test).reshape(-1, 1)

		Dlle_pion_data_test = np.array(Dlle_pion_data_test).reshape(-1, 1)
		Dllmu_pion_data_test = np.array(Dllmu_pion_data_test).reshape(-1, 1)
		Dllk_pion_data_test = np.array(Dllk_pion_data_test).reshape(-1, 1)
		Dllp_pion_data_test = np.array(Dllp_pion_data_test).reshape(-1, 1)
		Dlld_pion_data_test = np.array(Dlld_pion_data_test).reshape(-1, 1)
		Dllbt_pion_data_test = np.array(Dllbt_pion_data_test).reshape(-1, 1)

		#inputs_pion_data_test = np.concatenate((P_pion_data_test, Pt_pion_data_test , trackVertexX_pion_data_test, trackVertexY_pion_data_test, trackVertexZ_pion_data_test, rich1EntryX_pion_data_test, 
		#	rich1EntryY_pion_data_test, rich1ExitX_pion_data_test, rich1ExitY_pion_data_test, rich2EntryX_pion_data_test, rich2EntryY_pion_data_test, rich2ExitX_pion_data_test, rich2ExitY_pion_data_test), axis=1)

		inputs_pion_data_test = np.concatenate((P_pion_data_test, Pt_pion_data_test , trackVertexX_pion_data_test, trackVertexY_pion_data_test, trackVertexZ_pion_data_test), axis=1)
		Dll_pion_data_test = np.concatenate((Dlle_pion_data_test, Dllmu_pion_data_test, Dllk_pion_data_test, Dllp_pion_data_test, Dlld_pion_data_test, Dllbt_pion_data_test), axis=1)

		# Transform inputs/outputs

		print('Transforming inputs and outputs using Quantile Transformer...')
		
		inputs_pion_data_test = scaler_inputs.transform(inputs_pion_data_test)
		Dll_pion_data_test = scaler_Dll.transform(Dll_pion_data_test)

		# Map to reduced range

		inputs_pion_data_test = mapping(inputs_pion_data_test)
		Dll_pion_data_test = mapping(Dll_pion_data_test)

		# Producing testing data
		noise_list_test = np.random.normal(loc=mu, scale=sigma, size=[len(pion_data_test), self.params_noise])
		params_list_test = np.zeros((len(pion_data_test), self.params_physics))
		for e in range(len(pion_data_test)):
			params_list_test[e][0] = inputs_pion_data_test[e][0]
			params_list_test[e][1] = inputs_pion_data_test[e][1]
			params_list_test[e][2] = inputs_pion_data_test[e][2]
			params_list_test[e][3] = inputs_pion_data_test[e][3]
			params_list_test[e][4] = inputs_pion_data_test[e][4]
			#params_list_test[e][5] = inputs_pion_data_test[e][5]
			#params_list_test[e][6] = inputs_pion_data_test[e][6]
			#params_list_test[e][7] = inputs_pion_data_test[e][7]
			#params_list_test[e][8] = inputs_pion_data_test[e][8]
			#params_list_test[e][9] = inputs_pion_data_test[e][9]
			#params_list_test[e][10] = inputs_pion_data_test[e][10]
			#params_list_test[e][11] = inputs_pion_data_test[e][11]
			#params_list_test[e][12] = inputs_pion_data_test[e][12]

		obs_simu_test = np.zeros((len(pion_data_test), self.observables))
		obs_simu_test.fill(-1)
		for e in range(len(pion_data_test)):
			obs_simu_test[e][0] = Dll_pion_data_test[e][0]
			obs_simu_test[e][1] = Dll_pion_data_test[e][1]
			obs_simu_test[e][2] = Dll_pion_data_test[e][2]
			obs_simu_test[e][3] = Dll_pion_data_test[e][3]
			obs_simu_test[e][4] = Dll_pion_data_test[e][4]
			obs_simu_test[e][5] = Dll_pion_data_test[e][5]


		d_hist, g_hist = list(), list()

		print('Beginning training...')

		'''
		Training stage

		'''
		for epoch in range(1, epochs+1):

			print('-'*15, 'Epoch %d' % epoch, '-'*15)

			event_no_par = 0
			event_no_obs = 0

			if(epoch > 1):
				
				# shuffle data for training with number of epochs > 1
				print('Shuffling data...')
				shuffle_pion_data_train = np.concatenate((inputs_pion_data_train, Dll_pion_data_train), axis=1)
				np.random.shuffle(shuffle_pion_data_train)

				shuffle_pion_data_train = np.hsplit(shuffle_pion_data_train, [self.params_physics, self.params_physics + self.observables])

				inputs_pion_data_train = shuffle_pion_data_train[0]
				Dll_pion_data_train = shuffle_pion_data_train[1]


			for train_step in range(train_steps):

				log_mesg = '%d' % train_step
				noise_value = 0.05
				noise_list_discrim = np.random.normal(loc=mu,scale=sigma, size=[batch_size, self.params_noise])
				#noise_list_gen = np.random.normal(loc=mu,scale=sigma, size=[batch_size, self.params_noise])
				params_list_discrim = np.zeros([batch_size, self.params_physics])
				#params_list_gen = np.zeros([batch_size, self.params_physics])
				y_ones = np.ones([batch_size, 1])
				y_zeros = np.zeros([batch_size, 1])

				#inputs_pion_data_train_gen = random.choices(inputs_pion_data_train, k=batch_size)

				# add physics parameters to params_list_discrim
				for b in range(batch_size):
					params_list_discrim[b][0] = inputs_pion_data_train[event_no_par][0]
					params_list_discrim[b][1] = inputs_pion_data_train[event_no_par][1]
					params_list_discrim[b][2] = inputs_pion_data_train[event_no_par][2]
					params_list_discrim[b][3] = inputs_pion_data_train[event_no_par][3]
					params_list_discrim[b][4] = inputs_pion_data_train[event_no_par][4]
					#params_list_discrim[b][5] = inputs_pion_data_train[event_no_par][5]
					#params_list_discrim[b][6] = inputs_pion_data_train[event_no_par][6]
					#params_list_discrim[b][7] = inputs_pion_data_train[event_no_par][7]
					#params_list_discrim[b][8] = inputs_pion_data_train[event_no_par][8]
					#params_list_discrim[b][9] = inputs_pion_data_train[event_no_par][9]
					#params_list_discrim[b][10] = inputs_pion_data_train[event_no_par][10]
					#params_list_discrim[b][11] = inputs_pion_data_train[event_no_par][11]
					#params_list_discrim[b][12] = inputs_pion_data_train[event_no_par][12]
					event_no_par += 1

				params_list_discrim_copy = np.copy(params_list_discrim)

				# add physics parameters + noise to params_list
				#for b in range(batch_size):
				#	params_list_gen[b][0] = inputs_pion_data_train_gen[b][0]
				#	params_list_gen[b][1] = inputs_pion_data_train_gen[b][1]
				#	params_list_gen[b][2] = inputs_pion_data_train_gen[b][2]
				#	params_list_gen[b][3] = inputs_pion_data_train_gen[b][3]
				#	params_list_gen[b][4] = inputs_pion_data_train_gen[b][4]
					#params_list_gen[b][5] = inputs_pion_data_train_gen[b][5]
					#params_list_gen[b][6] = inputs_pion_data_train_gen[b][6]
					#params_list_gen[b][7] = inputs_pion_data_train_gen[b][7]
					#params_list_gen[b][8] = inputs_pion_data_train_gen[b][8]
					#params_list_gen[b][9] = inputs_pion_data_train_gen[b][9]
					#params_list_gen[b][10] = inputs_pion_data_train_gen[b][10]
					#params_list_gen[b][11] = inputs_pion_data_train_gen[b][11]
					#params_list_gen[b][12] = inputs_pion_data_train_gen[b][12]

				# Step 1
				# simulated observables (number 1)
				obs_simu = np.zeros((batch_size, self.observables))
				obs_simu.fill(-1)
				for b in range(batch_size):
					obs_simu[b][0] = Dll_pion_data_train[event_no_obs][0]
					obs_simu[b][1] = Dll_pion_data_train[event_no_obs][1]
					obs_simu[b][2] = Dll_pion_data_train[event_no_obs][2]
					obs_simu[b][3] = Dll_pion_data_train[event_no_obs][3]
					obs_simu[b][4] = Dll_pion_data_train[event_no_obs][4]
					obs_simu[b][5] = Dll_pion_data_train[event_no_obs][5]
					event_no_obs += 1

				obs_simu_copy = np.copy(obs_simu)

				# emulated DLL values
				obs_emul = self.generator.predict([noise_list_discrim, params_list_discrim])
				obs_emul_copy = np.copy(obs_emul)

				# decay the learn rate
				if(train_step % 1000 == 0 and train_step>0):
					decay_factor = 0.999
					discriminator_lr = K.eval(self.discriminator.optimizer.lr)
					K.set_value(self.discriminator.optimizer.lr, discriminator_lr*decay_factor)
					print('lr for Discriminator network updated from %f to %f' % (discriminator_lr, discriminator_lr*decay_factor))
					adversarial1_lr = K.eval(self.adversarial1.optimizer.lr)
					K.set_value(self.adversarial1.optimizer.lr, adversarial1_lr*decay_factor)
					print('lr for Adversarial1 network updated from %f to %f' % (adversarial1_lr, adversarial1_lr*decay_factor))

				# noise
				y_ones = np.array([np.random.uniform(0.7, 1.0) for x in range(batch_size)]).reshape([batch_size, 1])
				#y_zeros = np.array([np.random.uniform(0.00, 0.05) for x in range(batch_size)]).reshape([batch_size, 1])

				#if np.random.random() < noise_value:
				#	for b in range(batch_size):
				#		if np.random.random() < noise_value:
				#			obs_simu_copy[b], obs_emul_copy[b] = obs_emul[b], obs_simu[b]

				# train discriminator
				d_loss_simu = self.discriminator.train_on_batch([obs_simu_copy, params_list_discrim_copy], y_ones)
				d_loss_fake = self.discriminator.train_on_batch([obs_emul_copy, params_list_discrim_copy], y_zeros)
				d_loss = 0.5 * np.add(d_loss_simu, d_loss_fake)
				log_mesg = '%s [D loss: %f]' % (log_mesg, d_loss[0])

				y_ones = np.ones([batch_size, 1])

				#print(log_mesg)
				#print('--------------------')

				#noise_value*=0.999
				
				#Step 2
				
				# noise
				#for b in range(batch_size):
				#	if np.random.random() < noise_value:
				#		y_ones[b,0] = np.random.uniform(0.0, 0.01)

				# Add noise: flip labels when training the emulator

				y_ones = np.ones(shape=(batch_size, 1))
				for b in range(batch_size):
					if np.random.random() < noise_value:
						y_ones[b] = 0.0

				# train emulator
				
				a_loss = self.adversarial1.train_on_batch([noise_list_discrim, params_list_discrim], y_ones)
				log_mesg = '%s [G loss: %f]' % (log_mesg, a_loss[0])
				
				if(train_step % (1000) == 0):
					print('Epoch %d' % epoch)
					print(log_mesg)
					print('--------------------')

				noise_value*=0.999

				if(train_step % (1000) == 0 and train_step>0):

					print('Producing plots...')

					if not os.path.exists(output_path + '/output_%s/epoch_%s/iteration/train_step_%s/' % (train_no, epoch, train_step)):
						os.makedirs(output_path + '/output_%s/epoch_%s/iteration/train_step_%s/' % (train_no, epoch, train_step))

					# predict parameter values

					obs_emul_test = self.generator.predict([noise_list_test, params_list_test])

					# produce correlation and target plots (normalised and raw)

					plot_obs_emul_test_split = np.hsplit(obs_emul_test, self.observables)
					plot_obs_simu_test_split = np.hsplit(obs_simu_test, self.observables)

					# raw plots
					plot_obs_emul_test_raw = antimapping(obs_emul_test)
					plot_obs_simu_test_raw = antimapping(obs_simu_test)

					plot_obs_emul_test_raw = scaler_Dll.inverse_transform(plot_obs_emul_test_raw)
					plot_obs_simu_test_raw = scaler_Dll.inverse_transform(plot_obs_simu_test_raw)

					plot_obs_emul_test_raw_split = np.hsplit(plot_obs_emul_test_raw, self.observables)
					plot_obs_simu_test_raw_split = np.hsplit(plot_obs_simu_test_raw, self.observables)

					# produce true correlation plots
					plot_correlation(plot_obs_simu_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), plot_obs_simu_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(),
						plot_obs_simu_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), plot_obs_simu_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 
						plot_obs_simu_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), plot_obs_simu_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), train_step, epoch, 'DLL1', train_no)

					# produce emulated and simulated histograms superimposed (normalised)
					plot_DLL(plot_obs_simu_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), plot_obs_simu_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(),
						plot_obs_simu_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), plot_obs_simu_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 
						plot_obs_simu_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), plot_obs_simu_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), train_step, epoch, 'DLL1', train_no, 'norm')

					# produce emulated and simulated histograms superimposed (raw)
					plot_DLL(plot_obs_simu_test_raw_split[0].flatten(), plot_obs_emul_test_raw_split[0].flatten(), plot_obs_simu_test_raw_split[1].flatten(), plot_obs_emul_test_raw_split[1].flatten(),
						plot_obs_simu_test_raw_split[2].flatten(), plot_obs_emul_test_raw_split[2].flatten(), plot_obs_simu_test_raw_split[3].flatten(), plot_obs_emul_test_raw_split[3].flatten(), 
						plot_obs_simu_test_raw_split[4].flatten(), plot_obs_emul_test_raw_split[4].flatten(), plot_obs_simu_test_raw_split[5].flatten(), plot_obs_emul_test_raw_split[5].flatten(), train_step, epoch, 'DLL1', train_no, 'raw')


					# calculate deltsDLLs and plot	
					simu_diff = obs_emul_test - obs_simu_test

					simu_diff_split = np.hsplit(simu_diff, self.observables)
					
					Dlle_simu_diff = pd.Series(data=simu_diff_split[0].flatten(), name='RichDLLe')
					Dllmu_simu_diff = pd.Series(data=simu_diff_split[1].flatten(), name='RichDLLmu')
					Dllk_simu_diff = pd.Series(data=simu_diff_split[2].flatten(), name='RichDLLk')
					Dllp_simu_diff = pd.Series(data=simu_diff_split[3].flatten(), name='RichDLLp')
					Dlld_simu_diff = pd.Series(data=simu_diff_split[4].flatten(), name='RichDLLd')
					Dllbt_simu_diff = pd.Series(data=simu_diff_split[5].flatten(), name='RichDLLbt')

					plot_deltaDLL(Dlle_simu_diff, Dllmu_simu_diff, Dllk_simu_diff, Dllp_simu_diff, Dlld_simu_diff, Dllbt_simu_diff, train_step, epoch, 'DLL1', train_no)

					# make plot of pion efficiency as a function of momentum for different DLLk cuts

					# produce numpy array of momentum and DLLk distributions for emulated and simulated DLLk
					momentum_dep_array = np.column_stack((P_pion_data_test.flatten(), Dllk_pion_data_test.flatten(), plot_obs_emul_test_raw_split[2].flatten()))
					momentum_dep_df = pd.DataFrame(data=momentum_dep_array, columns=['TrackP', 'RichDLLk', 'GenRichDLLk'])

					# apply DLLk cuts to dataframe
					momentum_dep_df_DLLk0 = momentum_dep_df[ (momentum_dep_df.RichDLLk > 0) ]
					momentum_dep_df_DLLk5 = momentum_dep_df[ (momentum_dep_df.RichDLLk > 5) ]
					momentum_dep_df_GenDLLk0 = momentum_dep_df[ (momentum_dep_df.GenRichDLLk > 0) ]
					momentum_dep_df_GenDLLk5 = momentum_dep_df[ (momentum_dep_df.GenRichDLLk > 5) ]

					# produce plot of momentum dependence as a function of two different DLLk cut values
					plot_momentum_dep(momentum_dep_df_DLLk0, momentum_dep_df_GenDLLk0, momentum_dep_df_DLLk5, momentum_dep_df_GenDLLk5, momentum_dep_df, train_no, epoch, train_step)

					print("Overlaps:")
					print("DLLe = ", histogram_intersection(plot_obs_simu_test_split[0].flatten(), plot_obs_emul_test_split[0].flatten(), 750, [-1,1]))
					print("DLLmu = ", histogram_intersection(plot_obs_simu_test_split[1].flatten(), plot_obs_emul_test_split[1].flatten(), 750, [-1,1])) 
					print("DLLk = ", histogram_intersection(plot_obs_simu_test_split[2].flatten(), plot_obs_emul_test_split[2].flatten(), 750, [-1,1])) 
					print("DLLp = ", histogram_intersection(plot_obs_simu_test_split[3].flatten(), plot_obs_emul_test_split[3].flatten(), 750, [-1,1])) 
					print("DLLd = ", histogram_intersection(plot_obs_simu_test_split[4].flatten(), plot_obs_emul_test_split[4].flatten(), 750, [-1,1])) 
					print("DLLbt = ", histogram_intersection(plot_obs_simu_test_split[5].flatten(), plot_obs_emul_test_split[5].flatten(), 750, [-1,1]))

				if(train_step == train_steps - 1 or train_step == 0):
					# record history every 1000 iterations and summarise
					d_hist.append(d_loss[0])
					g_hist.append(a_loss[0])
					plot_history(d_hist, g_hist, train_no)
		# save weights
		if(save_weights):
			self.save_weights(output_path, train_no)

		exit(0)
		
if __name__ == '__main__':

	cgan = ConditionalGANPID(params_physics=args.params_physics, params_noise=args.params_noise, observables=args.observables, print_summary=args.print_summary)

	if args.train:
		cgan.train(train_steps=args.train_steps, batch_size=args.batch_size, train_no=args.train_no, epochs=args.epochs, data_path=args.data_path, save_weights=args.save_weights, output_path=args.output_path)

